#!/system/bin/sh

setprop persist.asuslog.dump.date ""
TIME=`date +%Y_%m%d_%H%M%S`
setprop persist.asuslog.dump.date $TIME
datecmd="getprop persist.asuslog.dump.date"
LOG_DATE=`$datecmd`
idx=0
#LOG_DATE="test"
while [ $LOG_DATE != $TIME -a $idx != 10 ] 
do 
  idx=$(($idx+1))
  #echo "$idx" >> "/sdcard/aaa.txt"
  TIME=`date +%Y_%m%d_%H%M%S`
  setprop persist.asuslog.dump.date $TIME
  LOG_DATE=`$datecmd`
done

