#!/system/bin/sh


HISORY_PATH="/data/history.txt"
APP_DIR="/data/data/com.asus.fdclogtool/files"
setprop persist.asuslog.main.enable   0
setprop persist.asuslog.kernel.enable 0
setprop persist.asuslog.events.enable 0
setprop persist.asuslog.radio.enable  0

mount -o rw,remount /system && echo "mount_socuss" > $HISORY_PATH
echo "mount_finish" >> $HISORY_PATH

echo "start" >> $HISORY_PATH
xbins=(tcpdump procrank)
for name in ${xbins[@]}; do
 if [  -e "/system/xbin/$name" ]; then
    echo "$name exist" >> $HISORY_PATH
  else
    echo "$name no exist,coping \r\n" >> $HISORY_PATH
    cp "$APP_DIR/$name" "/system/xbin/$name"
    chmod 6755 "/system/xbin/$name" && echo "/system/xbin/$name change chmod success \r\n" >> $HISORY_PATH
    
 fi
done

bins=(asuslogcat )
for name in ${bins[@]}; do
 if [  -e "/system/bin/$name" ]; then
    echo "$name exist" >> $HISORY_PATH
  else
    echo "$name no exist,coping \r\n" >> $HISORY_PATH
    cp "$APP_DIR/$name" "/system/bin/$name"
    chmod 6755 "/system/bin/$name" && echo "/system/bin/$name change chmod success \r\n" >> $HISORY_PATH
   ls -l "/system/bin/$name" >> $HISORY_PATH
 fi
done


mkdir -p "/data/debug/"
# for 4.3 image no 4.4,because 4.4 buildin 
datecmd="getprop ro.build.version.sdk"
image_sdk=`$datecmd`
 echo "image_sdk $image_sdk \r\n" >> $HISORY_PATH
if [ "$image_sdk" == "18" ]; then
    etcs=(packlogs.sh )
    for name in ${etcs[@]}; do
     if [  -e "/system/etc/$name" ]; then
         echo "$name  exist,repeat coping \r\n" >> $HISORY_PATH
         cp "$APP_DIR/$name" "/system/etc/$name"
          chmod 6755 "/system/etc/$name" && echo "/system/etc/$name change chmod success \r\n" >> $HISORY_PATH
    
         ls -l "/system/etc/$name" >> $HISORY_PATH
    else
        echo "$name no exist,coping \r\n" >> $HISORY_PATH
        cp "$APP_DIR/$name" "/system/etc/$name"
       chmod 6755 "/system/etc/$name" && echo "/system/etc/$name change chmod success \r\n" >> $HISORY_PATH
    
      ls -l "/system/etc/$name" >> $HISORY_PATH
   fi
   done
else
     echo "no 4.3,no copy packlogs.sh \r\n" >> $HISORY_PATH	
fi
getprop ro.build.version.sdk
datas=(busybox )
for name in ${datas[@]}; do
 if [  -e "/data/debug/$name" ]; then
    echo "$name exist" >> $HISORY_PATH
  else
    echo "$name no exist,coping \r\n" >> $HISORY_PATH
    cp "$APP_DIR/$name" "/data/debug/$name"
    chmod 4777 "/data/debug/$name" && echo "/data/debug/$name change chmod success \r\n" >> $HISORY_PATH
    ls -l "/data/debug/$name" >> $HISORY_PATH
 fi
done

setprop persist.asuslog.set.date 1
setprop persist.asuslog.savedir "/sdcard/Asuslog/"
setprop persist.asuslog.dump.date ""
TIME=`date +%Y_%m%d_%H%M%S`
setprop persist.asuslog.dump.date $TIME


datecmd="getprop persist.asuslog.dump.date"
LOG_DATE=`$datecmd`

while [ "${#LOG_DATE}" == "0" ] 
do 
   sleep 0.3
   LOG_DATE=`$datecmd`
done 



setprop persist.asuslog.main.enable 1
setprop persist.asuslog.kernel.enable 1
setprop persist.asuslog.events.enable 1
setprop persist.asuslog.radio.enable 1

setprop persist.asuslog.prevrootpath "/sdcard/Asuslog/$LOG_DATE/"
chmod 777 $HISORY_PATH
am broadcast -a "com.asus.init.log.completed"
setprop persist.asuslog.dump.enable 1
echo "init log finish" >> $HISORY_PATH
