#!/system/bin/sh


HISORY_PATH="/data/history.txt"
APP_DIR="/data/data/com.asus.fdclogtool/files"
LOGFILE ="/data/Asuslog/version.txt"

mkdir -p $APP_DIR
mkdir -p "/data/Asuslog/"
cp "/system/etc/init_log_date.sh" "$APP_DIR/init_log_date.sh"
cp "/system/bin/logdumps" "$APP_DIR/logdumps"
chmod 4777 "$APP_DIR/logdumps"


echo "min_os" > "/data/Asuslog/version.txt"
getprop "ro.build.display.id" >> "/data/Asuslog/version.txt"

setprop persist.asuslog.savedir "/data/Asuslog/"
setprop persist.asuslog.dump.date ""
TIME=`date +%Y_%m%d_%H%M%S`
setprop persist.asuslog.dump.date $TIME


datecmd="getprop persist.asuslog.dump.date"
LOG_DATE=`$datecmd`

while [ "${#LOG_DATE}" == "0" ] 
do 
   sleep 0.3
   LOG_DATE=`$datecmd`
done 



setprop persist.asuslog.main.enable 1
setprop persist.asuslog.kernel.enable 1
setprop persist.asuslog.events.enable 1
setprop persist.asuslog.radio.enable 1
setprop persist.asuslog.facmodeminit 1
setprop persist.asuslog.prevrootpath "/data/Asuslog/$LOG_DATE/"


setprop persist.asuslog.fac.init 0


