package com.asus.log;

import java.io.File;

import android.app.Activity;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

import com.asus.tool.DebuggerMain;
import com.asus.tool.DumpSyslog;
import com.asus.tool.IServiceLog;
import com.asus.tool.Settings;
import com.asus.tool.Util;
import com.asus.fdclogtool.R;
public class NetWorkLog extends BaseLog implements OnCheckedChangeListener{

	public static final String KEY_TCPDUMP="persist.asuslog.tcpdump.enable";
	private static final String TAG = "TcpDumpLog";
	private Switch mTcpLogSwitch;
	private Switch mWifiSwitch;
	private Switch mWifiFirmwareSwitch;
	private String WIFI_FIRMWARE_PATH="/sys/module/bcm43362/parameters/dhd_console_ms";
	public NetWorkLog(Activity activity, View view) {
		super(activity, view);
		mTcpLogSwitch=(Switch) view.findViewById(R.id.switch_tcpdump_id);
		mWifiSwitch=(Switch) view.findViewById(R.id.switch_wifi_id);
		mWifiFirmwareSwitch=(Switch) view.findViewById(R.id.switch_wifi_firmware_id);
		mTcpLogSwitch.setChecked(getPropCheck(KEY_TCPDUMP));
		mTcpLogSwitch.setOnCheckedChangeListener(this);
		mWifiSwitch.setChecked(Settings.isWifiEnable());
		mWifiSwitch.setOnCheckedChangeListener(this);
		
		mWifiFirmwareSwitch.setOnCheckedChangeListener(this);
		view.findViewById(R.id.wifi_firmware_layout).setOnClickListener(this);
		view.findViewById(R.id.tcpdump_layout).setOnClickListener(this);
		view.findViewById(R.id.wifi_layout).setOnClickListener(this);
	}
	
	public void onResume() {
		 if(mWifiFirmwareSwitch!=null){
			 mWifiFirmwareSwitch.setChecked(readFirmWareState());
		 }
	 }
	
	public boolean readFirmWareState(){
		File file=new File(WIFI_FIRMWARE_PATH);
		int value=0;
		if(file.exists()){
			try {
				 value=Integer.valueOf(Util.getFileText(WIFI_FIRMWARE_PATH));
			} catch (Exception e) {
				Toast.makeText(mActivity, "wifi config error value="+e.getMessage(), Toast.LENGTH_SHORT).show();
			}
			
		}
		if(value==0){
			return false;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.tcpdump_layout:
			log("switch_tcpdump_id=");
			boolean state=mTcpLogSwitch.isChecked();
			mTcpLogSwitch.setChecked(state=!state);
		
			break;
		case R.id.wifi_layout:
			log("switch_tcpdump_id=");
			state=mWifiSwitch.isChecked();
			mWifiSwitch.setChecked(state=!state);
		
			break;
		case R.id.wifi_firmware_layout:
			state=mWifiFirmwareSwitch.isChecked();
			log("switch_wifi_firmware_id state="+!state);
			mWifiFirmwareSwitch.setChecked(state=!state);
		
			break;
		default:
			break;
		}
	}

	 public static void forceStop(){
		 setPropCheck(KEY_TCPDUMP, false);
		 Settings.setWifiEnable(false);
	 }
	
	@Override
	public void onSelectAll() {
		// TODO Auto-generated method stub
		super.onSelectAll();
		mTcpLogSwitch.setChecked(true);
		mWifiSwitch.setChecked(true);
	}

	public void onSetDefault(){
		mTcpLogSwitch.setChecked(false);
		mWifiSwitch.setChecked(true);
	}
	
	@Override
	public void onCancelAll() {
		// TODO Auto-generated method stub
		super.onCancelAll();
		mTcpLogSwitch.setChecked(false);
		mWifiSwitch.setChecked(false);
	}
	
	public static void log(String message){
		Log.v(TAG, message);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(isChecked)
		{
			if(isDiskValidShowToast()==false){
				buttonView.setOnCheckedChangeListener(null);
				buttonView.setChecked(false);
				buttonView.setOnCheckedChangeListener(this);
				return;
			}
		}
		if(buttonView==mTcpLogSwitch){
			setPropCheck(KEY_TCPDUMP, isChecked);
		}
		if(buttonView==mWifiSwitch){
			Settings.setWifiEnable(isChecked);
			try {
				mServiceLog.wifiLogEnable(isChecked);
			} catch (RemoteException e) {
				
				Log.e(DebuggerMain.TAG, "error="+e.getMessage());
			}
		}
		if(buttonView==mWifiFirmwareSwitch){
			if(isChecked){
				Util.setCmd("echo 250 > "+WIFI_FIRMWARE_PATH);
			}else{
				Util.setCmd("echo 0 > "+WIFI_FIRMWARE_PATH);
			}
			
		}
	}
}
