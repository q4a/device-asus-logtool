package com.asus.loguploader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemProperties;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import com.asus.loguploader.utils.FileUtils;

import android.app.Notification;

import android.view.LayoutInflater;
import android.widget.RadioGroup;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.WindowManager;
import com.asus.fdclogtool.R;
import java.util.ArrayList; //Joseph_Lin 20140117+
public class LogUploaderService extends Service {
    public static final String TAG = "LogUploaderService";

    private String mMonitorStartTime = "18:0";
    private String mMonitorStopTime = "6:0";

    private static final String logDirectory = Environment.getExternalStorageDirectory().toString() + "/ASUS/LogUploader";
    private static final String ASUSDirectory = Environment.getExternalStorageDirectory().toString() + "/ASUS";

    private Context mContext;

    AlarmManager mAlarmManager;
    PowerManager.WakeLock mWakeLock;

    boolean mIsRegisterBatteryReceiver = false;
    boolean mIsCharging = false;
    boolean mIsRegisterConnectivityReceiver = false;
    boolean mIsWifiOn = false;
    boolean mIs3gOn = false;
    boolean mIsRegisterPackLogsReceiver = false;

    boolean mNetworkPref3gEnable = false;

    FileUtils mFileUtils;
    boolean mManualUpload = false;
    HttpMultipartPostAsyncTask mPostTask;

    private Object mLock = new Object();

    private static final IntentFilter INTENT_FILTER_FOR_BATTERY_CHANGE = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

    public static int STATUS_LOG_CREATER_PROCESSING = 0 ;
    public static int STATUS_LOG_CREATER_COMPLETE = 1 ;

    HistoryFile mHistoryFile;

    AlertDialog mLoadPreferenceDialog;
    private WifiSignalStrengthReceiver mWifiSignalStrengthReceiver = null;
    private MobileSignalStrengthListener mMobileSignalStrengthListener = null;
	
	int flag = 0; //Joseph_Lin 20140117+

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LogUploaderServiceBinder extends Binder {
        LogUploaderService getService() {
            return LogUploaderService.this;
        }
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        mContext = getApplicationContext();

        mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        setAlarmTime();
        setAlarm(mMonitorStartTime);
//        setAlarm(mMonitorStopTime);  // useless function by joey_lee@asus.com

        IntentFilter s_intentFilter = new IntentFilter();
        s_intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        s_intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        registerReceiver(m_timeChangedReceiver, s_intentFilter);

        registerReceiver();

        mHistoryFile = new HistoryFile(mContext);
        createLogDirIfNotExists();

      
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand LogUploaderService" + intent);
        Log.d(TAG, "logDirectory: " + logDirectory);
        if(intent == null)
            return START_STICKY;

        AlarmAlertWakeLock.acquireCpuWakeLock(mContext);

        String action = intent.getAction();

        showNetworkInfo();  // Joey_Lee
        if ("start-monitor".equals(action)) {
            createLogFile();
            //registerReceiver();
        } else if ("stop-monitor".equals(action)) {
            //unregisterReceiver();
        } else if ("manual-action".equals(action)) {
            final int mReason = intent.getIntExtra("reason",0);
            final String mDescription = intent.getStringExtra("description");
            final String mReporter = intent.getStringExtra("reporter");
            new Thread()
            {
                @Override
                public void run() {
                    createLogFile(mReason,mDescription, mReporter);
                    long lastManualTime = SystemClock.elapsedRealtime() ;
                    saveLastManualTime(lastManualTime);
                    mManualUpload = true;		
					//Joseph_Lin 20140117+
					flag = 1;					
					//Joseph_Lin 20140117-
                }
            }.start();
        } else if ("stop-foreground".equals(action)) {
            Log.d(TAG, "service stop foreground!!");
            stopForeground(true);
        } else if ("reset-autoupload".equals(action)) {  // for unit test: auto upload by joey_lee@asus.com
            String resetTime = intent.getStringExtra("resetTime");
            int split = resetTime.indexOf(":");
            if (split < 0) {
                Log.d(TAG, "LogUploader reset auto upload time format error");
                Toast.makeText(this, "LogUploader reset auto upload time format error", Toast.LENGTH_LONG).show();
            } else {
                try {
                    String hour = resetTime.substring(0, split);
                    String minute = resetTime.substring(split + 1, resetTime.length());
                    if (Integer.valueOf(hour) < 0 || Integer.valueOf(hour) >= 24
                            || Integer.valueOf(minute) < 0 || Integer.valueOf(minute) >= 60) {
                        Log.d(TAG, "LogUploader reset auto upload time error: out of range");
                        Toast.makeText(this, "LogUploader reset auto upload time error: out of range", Toast.LENGTH_LONG).show();
                    } else {
                        resetTime = String.valueOf(Integer.valueOf(hour)) + ":" + String.valueOf(Integer.valueOf(minute));
                        Log.d(TAG, "LogUploader reset auto upload time from " + mMonitorStartTime + " to " + resetTime);
                        Toast.makeText(this, "LogUploader reset auto upload time from " + mMonitorStartTime + " to " + resetTime, Toast.LENGTH_LONG).show();
                        mMonitorStartTime = resetTime;
                        setAlarm(mMonitorStartTime);
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.d(TAG, "LogUploader reset auto upload time error: NumberFormatException: " + e.toString());
                    Toast.makeText(this, "LogUploader reset auto upload time error: NumberFormatException", Toast.LENGTH_LONG).show();
                }
            }
        }

        AlarmAlertWakeLock.releaseCpuLock();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LogUploaderServiceBinder();

    private String getIMEI(){
        TelephonyManager telephonyManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    private void setAlarmTime(){
        String mIMEI = getIMEI();
        if ( mIMEI == null )
            return;
        int mMin, mHour;
        try {
            mMin = ((Integer.valueOf(mIMEI.substring(13,15))) & 0x7)*15;
            mHour = 18;

            if ( mMin >= 60){
                mHour = mHour + (mMin/60);
                mMin = mMin % 60;
            }

            mMonitorStartTime = Integer.toString(mHour) + ":" + Integer.toString(mMin);

        } catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }

        Log.e(TAG,"mMonitorStartTime = " + mMonitorStartTime + " ; "  + "mMonitorStopTime = " + mMonitorStopTime);
    }

    private void setAlarm(String time) {
        Log.d(TAG, "set Alarm to " + time);
        String[] pieces; int hourOfDay; int minute;
        pieces=time.split(":");
        hourOfDay = Integer.parseInt(pieces[0]);
        minute = Integer.parseInt(pieces[1]);

        Calendar cal_alarm = Calendar.getInstance();
        Calendar cal_now = Calendar.getInstance();
        cal_alarm.setTimeInMillis(System.currentTimeMillis());
        cal_alarm.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal_alarm.set(Calendar.MINUTE, minute);
        cal_alarm.set(Calendar.SECOND, 0);
        cal_alarm.set(Calendar.MILLISECOND, 0);

        if(cal_alarm.before(cal_now)){
            //if its in the past increment
            cal_alarm.add(Calendar.DATE,1);
        }

        Intent intent;
        intent = new Intent(mContext,LogUploaderAlarmReceiver.class);
        int requestCode = 0;

        if(time.equals(mMonitorStartTime)){
            intent.putExtra("upload-action", "start-monitor");
            requestCode = 1;
        } else if (time.equals(mMonitorStopTime)){
            intent.putExtra("upload-action", "stop-monitor");
            requestCode = 2;
        }

//        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, intent, 0);  // cancel previous alarm first by joey_lee@asus.com
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.cancel(pendingIntent);
        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), (24 * 60 * 60 * 1000),pendingIntent);
    }

    BroadcastReceiver mBatteryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean changed = updateBatteryCharginState(intent);
            if (changed) {
                Log.d(TAG, "charging state changed");
                checkIfNeedToUploadLog();
            }
        }
    };

    private void registerBatteryReceiver() {
        if (!mIsRegisterBatteryReceiver) {
             registerReceiver(mBatteryReceiver, INTENT_FILTER_FOR_BATTERY_CHANGE);
             mIsRegisterBatteryReceiver = true;
        }
    }

    private void unregisterBatteryReceiver() {
        if (mIsRegisterBatteryReceiver) {
            unregisterReceiver(mBatteryReceiver);
            mIsRegisterBatteryReceiver = false;
        }
        mIsCharging = false;
    }

    private boolean updateBatteryCharginState(Intent intent) {
        boolean phoneChargingState = (intent.getIntExtra("plugged", 0) & (BatteryManager.BATTERY_PLUGGED_AC | BatteryManager.BATTERY_PLUGGED_USB)) != 0;
        boolean changed = false;
        boolean charging = phoneChargingState;
        if (mIsCharging != charging) {
            // apply current change
            Log.d(TAG, "charging:" + charging);
            mIsCharging = charging;
            changed = true;
        }
        return changed;
    }

    /** Receiver for ConnectivityManager events */
    private BroadcastReceiver mConnectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean changed = updateNetworkState(intent);
            if (changed) {
                Log.d(TAG, "network state changed");
                checkIfNeedToUploadLog();
            }
        }
    };

    private void registerConnectivityReceiver() {
        if (!mIsRegisterConnectivityReceiver){
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(mConnectivityReceiver, intentFilter);
            mIsRegisterConnectivityReceiver = true;
        }
    }

    private void unregisterConnectivityReceiver() {
        if (mIsRegisterConnectivityReceiver) {
            unregisterReceiver(mConnectivityReceiver);
            mIsRegisterConnectivityReceiver = false;
        }
        mIsWifiOn = false;
    }

    private boolean updateNetworkState(Intent intent){
        boolean mWifiState = false;
        boolean m3gState = false;
        boolean changed = false;
        String action = intent.getAction();
        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
            // There is connectivity
            NetworkInfo netInfo = (NetworkInfo)intent.getParcelableExtra(
                    ConnectivityManager.EXTRA_NETWORK_INFO);
            if (netInfo != null) {
                // Verify that it's a WIFI connection
                if (netInfo.getState() == NetworkInfo.State.CONNECTED &&
                        netInfo.getType() == ConnectivityManager.TYPE_WIFI ) {
                    mWifiState = true;
                } else if (netInfo.getState() == NetworkInfo.State.DISCONNECTED &&
                        netInfo.getType() == ConnectivityManager.TYPE_WIFI ){
                    mWifiState = false;
                }

                // Verify that it's a 3G connection
                if (netInfo.getState() == NetworkInfo.State.CONNECTED &&
                        netInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                    m3gState = true;
                } else if (netInfo.getState() == NetworkInfo.State.DISCONNECTED &&
                        netInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                    m3gState = false;
                }

                if (mIsWifiOn != mWifiState){
                    Log.d(TAG, "wifi is connected:" + mWifiState);
                    mIsWifiOn = mWifiState;
                    changed = true;
                }

                if (mIs3gOn != m3gState){
                    Log.d(TAG, "3g is connected:" + m3gState);
                    mIs3gOn = m3gState;
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * show more network information by Joey_Lee
     */
    private void showNetworkInfo() {
        Log.d(TAG, "########## showNetworkInfo() ##########");
        getNetworkInfo();
        getLocalIpAddress();
    }

    /**
     * get network type by Joey_Lee
     */
    private void getNetworkInfo() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            Log.d(TAG, "getSystemService(Context.CONNECTIVITY_SERVICE) failed");
        }

        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info != null) {
            NetworkInfo.State state = info.getState();
            Log.d(TAG, "network state: " + state.toString());
            Log.d(TAG, "network type: " + info.getTypeName());

            if (state == NetworkInfo.State.CONNECTED) {
                if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                    getWifiSignalStrength();
                } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                    getMobileSignalStrength();
                }
            }
        } else {
            Log.d(TAG, "network type: NO NETWORK!!!");
        }
    }

    /**
     * get local IP address by Joey_Lee
     */
    private void getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
//                  appendLog("Player","IpAddress = " + inetAddress.getHostAddress());

                    //avoid getting MAC address with ipv6 format on Android 3.0
                    if (!inetAddress.isLoopbackAddress() && inetAddress.getHostAddress().indexOf(":") == -1) {
                        Log.d(TAG, "Local IP address: " + inetAddress.getHostAddress());
                    }
                }
            }
        } catch(SocketException e) {
            Log.d(TAG, "SocketException: " + e.toString());
        } catch (NoSuchElementException e) {
            Log.d(TAG, "NoSuchElementException: " + e.toString());
        }
    }

    /**
     * Wifi signal strength receiver by Joey_Lee
     */
    private class WifiSignalStrengthReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            int state = wifi.getWifiState();
            if (state == WifiManager.WIFI_STATE_ENABLED) {
                List<ScanResult> results = wifi.getScanResults();

                for (ScanResult result : results) {
                    if(result.BSSID.equals(wifi.getConnectionInfo().getBSSID())) {
                        int rssi = wifi.getConnectionInfo().getRssi();
                        int level = WifiManager.calculateSignalLevel(rssi, result.level);
                        int difference = level * 100 / result.level;
                        int signalStrangth= 0;
                        if (difference >= 100) {
                            signalStrangth = 4;
                        } else if (difference >= 75) {
                            signalStrangth = 3;
                        } else if (difference >= 50) {
                            signalStrangth = 2;
                        } else if (difference >= 25) {
                            signalStrangth = 1;
                        }
                        Log.d(TAG, "Wifi signal strength info:");
                        Log.d(TAG, "Rssi: " + rssi);
                        Log.d(TAG, "Difference: " + difference);
                        Log.d(TAG, "signal state: " + signalStrangth);

                        if(mWifiSignalStrengthReceiver != null) {  // just get info one time
                            unregisterReceiver(mWifiSignalStrengthReceiver);
                            mWifiSignalStrengthReceiver = null;
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * get Wifi signal strength by Joey_Lee
     */
    private void getWifiSignalStrength() {
        if (mWifiSignalStrengthReceiver == null) {
            mWifiSignalStrengthReceiver = new WifiSignalStrengthReceiver();
            IntentFilter intentFilter = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
            registerReceiver(mWifiSignalStrengthReceiver, intentFilter);
        }
    }

    /**
     * mobile signal strength listener by Joey_Lee
     */
    private class MobileSignalStrengthListener extends PhoneStateListener {
        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            int strength  = signalStrength.getGsmSignalStrength();
            Log.d(TAG, "mobile signal strength info:");
            Log.d(TAG, "strength: " + strength);

            if (strength > 30) {
                Log.d(TAG, "signal strength GOOD");
            } else if (strength > 20 && strength < 30) {
                Log.d(TAG, "signal strength AVERAGE");
            } else if (strength < 20) {
                Log.d(TAG, "signal strength WEAK");
            }

            if (mMobileSignalStrengthListener != null) { // just get info one time
                TelephonyManager telephonyManager  = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                telephonyManager.listen(mMobileSignalStrengthListener ,PhoneStateListener.LISTEN_NONE);
            }
        }
    };

    /**
     * get mobile signal strength by Joey_Lee
     */
    private void getMobileSignalStrength() {
        try {
            if (mMobileSignalStrengthListener == null) {
                mMobileSignalStrengthListener = new MobileSignalStrengthListener();
                TelephonyManager telephonyManager  = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                telephonyManager.listen(mMobileSignalStrengthListener ,PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
            }
        } catch (Exception e) {
            Log.d(TAG, "new MobileSignalStrengthListener() error: " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * install busybox if necessary from LogTool by Joey_Lee
     */
    public static boolean checkBusybox(final Context context) {
        if (isMonkeyRunning()) {
            return false;
        }
        SystemProperties.set("persist.asus.debug", "1");
        if(isFileExisted("/system/etc/busybox") ) 
        {
            Log.e(TAG, "busybox existed \n");
            return false;
        } else
        {
            AssetManager assets = context.getAssets();
            FileOutputStream outputStream = null,outputStream1 = null;
            InputStream inputStream = null,inputStream1 = null;
            Log.d(TAG, "Check busybox");
            byte buf[] = new byte [1024];
            int len=0;
            try {
                inputStream = assets.open("busybox");
                inputStream1 = assets.open("busybox");
                File defDirFile = new File("/system/etc");
              
                boolean existed = defDirFile.exists();
                
                if (!existed) {
                    Log.e(TAG, "no /data/debug folder\n");
                    defDirFile.mkdirs();
                }
                Log.e(TAG, "the directory has been already existed\n");
                Log.d(TAG, "copy busybox\n");
               
                len = 0;
                outputStream1 = new FileOutputStream("/system/etc/busybox");
                while ((len = inputStream1.read(buf)) != -1) {
                    outputStream1.write(buf, 0, len);
                }
                outputStream1.close();

                inputStream.close();
                
               
                File busyboxFile1 = new File("/system/etc/busybox");
                busyboxFile1.setExecutable(true, false);
                busyboxFile1.setReadable(true, false);
                busyboxFile1.setWritable(true, false);

                //File everbootupFile = new File("/data/everbootup");
                //everbootupFile.delete();
                SystemProperties.set("persist.asus.debug", "1");

                Toast.makeText(context, "Install busybox success, reboot in 5 seconds", Toast.LENGTH_LONG).show();
                Handler handler = new Handler();
                
                Runnable action = new Runnable() {
                    @Override
                    public void run() {
                        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                        if (powerManager != null) {
                            powerManager.reboot("reboot");
                            
                        }
                    }
                };
                handler.postDelayed(action, 5000);
                return true;
            } catch (IOException e) {
                Log.d(TAG, "IOException: " + e.toString());
            }
        }
		return false;
    }

    /**
     * check if monkey is running from LogTool by Joey_Lee
     */
    private static boolean isMonkeyRunning() {
        if(ActivityManager.isUserAMonkey()) {
            Log.i(TAG, "Monkey is running!!");
            return true;
        } else {
            Log.i(TAG, "Monkey is not running!!");
            return false;
        }
    }

    /**
     * check if file exists from LogTool by Joey_Lee
     */
    public static boolean isFileExisted(String filePath) {
        File f = new File(filePath);
        boolean existed = f.exists();
        if (existed) {
            Log.d(TAG, filePath + " is existed");
            return true;
        } else {
            Log.d(TAG, filePath + " is not existed");
            return false;
        }
    }

    /** Receiver for PackLogs Complete */
    private BroadcastReceiver mPackLogsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "PackLogs Complete");

            sendLogCreaterBroadcast(STATUS_LOG_CREATER_COMPLETE);
            saveStatus(STATUS_LOG_CREATER_COMPLETE);

            SharedPreferences firstSend = getSharedPreferences ("FIRST_SEND", 0);
            boolean showDiag = firstSend.getBoolean("FIRST_SEND_FLAG", true);
            if (mManualUpload) {
                if (showDiag) {
                    showUploadDialogAndUploadNow();
                } else {
//                    String filename = getLastModifiedFileName();
//                    uploadLogNow(filename);
                    // because of splitting the upload file, cannot upload only one file by joey_lee@asus.com
                    uploadLogNow(logDirectory);
                }
                mManualUpload = false;
            } else {
                //checkIfNeedToUploadLog();
            }

            releasePartialWakeLock();
        }
    };

    private void registerPackLogsReceiver() {
        if (!mIsRegisterPackLogsReceiver){
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("com.asus.packlogs.completed");
            registerReceiver(mPackLogsReceiver, intentFilter);
            mIsRegisterPackLogsReceiver = true;
        }
    }

    private void unregisterPackLogsReceiver() {
        if (mIsRegisterPackLogsReceiver) {
            unregisterReceiver(mPackLogsReceiver);
            mIsRegisterPackLogsReceiver = false;
        }
    }

    private void registerReceiver(){
        Log.d(TAG, "register receiver !!");
        registerBatteryReceiver();
        registerConnectivityReceiver();
        registerPackLogsReceiver();
    }

    private void unregisterReceiver(){
        Log.d(TAG, "unregister receiver !!");
        unregisterBatteryReceiver();
        unregisterConnectivityReceiver();
        unregisterPackLogsReceiver();
    }

    private final BroadcastReceiver m_timeChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(Intent.ACTION_TIME_CHANGED) ||
                action.equals(Intent.ACTION_TIMEZONE_CHANGED))
            {
                saveLastManualTime(0);
            }
        }
    };

    private void checkIfNeedToUploadLog(){
        mHistoryFile.writeDebugLog("checkIfNeedToUploadLog: wifi=" + mIsWifiOn + " ,3G=" + mIs3gOn + " ,charger=" + mIsCharging);

        // from PreferenceActivity
        SharedPreferences networkpref = PreferenceManager.getDefaultSharedPreferences(this);
        mNetworkPref3gEnable = networkpref.getBoolean("setting_pref_3gEnable", true);

        SharedPreferences firstSend = getSharedPreferences ("FIRST_SEND", 0);
        boolean showDiag = firstSend.getBoolean("FIRST_SEND_FLAG", true);
		//Joseph_Lin 20140117+		
        //if(mIsCharging && mIsWifiOn && !mNetworkPref3gEnable) {
		if(mIsWifiOn && !mNetworkPref3gEnable) {
		//Joseph_Lin 20140117-
            if(showDiag) {
                showUploadDialogAndUploadNow();
            } else {
                uploadLogNow();
            }
		//Joseph_Lin 20140117+
        //} else if (mIsCharging && (mIsWifiOn || mIs3gOn) && mNetworkPref3gEnable) {
		} else if ((mIsWifiOn || mIs3gOn) && mNetworkPref3gEnable) {
		//Joseph_Lin 20140117-
            if(showDiag) {
                showUploadDialogAndUploadNow();
            } else {
                uploadLogNow();
            }
        }
    }
	
	//Joseph_Lin 20140117+
	// return false if log files are cleared, otherwise return true
    private boolean checkIfLogFileExistence() {
        Log.d(TAG,"checkLogFileExistence()");
        List<String> uploadFileList = new ArrayList<String>();
        File directory = new File(logDirectory);
        File[] files = directory.listFiles();

        try {
            if (files != null && files.length >0){
                for (int i = 0; i < files.length; ++i) {
                    uploadFileList.add(files[i].getAbsolutePath());
                }
            }

            for(String filepath:uploadFileList) {
                Log.d(TAG, "candidates of upload _file: " + filepath);
                String [] token;
                token = filepath.split("/");
                String filename = token[token.length-1];

                String [] tail;
                tail = filename.split("\\.");
                if (tail[tail.length-1].equals("gz")) {
                    uploadFileList.clear();
                    uploadFileList = null;
                    directory = null;
                    files = null;
                    return true;
                } else {
                    // because of splitting the upload file by joey_lee@asus.com
                    if (tail.length >= 3 && tail[tail.length - 3].equals("gz")) {
                        uploadFileList.clear();
                        uploadFileList = null;
                        directory = null;
                        files = null;
						return true;
                    }
                }
            }
        } catch(Exception e) {
            Log.d(TAG, "Exception: " + e.getMessage());
        }

        uploadFileList.clear();
        uploadFileList = null;
        directory = null;
        files = null;
        return false;
    }
	//Joseph_Lin 20140117-

    private void uploadLogNow(){
        //Harrison, add switch in development settings for auto upload
		//Joseph_Lin 20140117+
        //-if (SystemProperties.getInt("persist.asus.autoupload.enable", 0) != 1)
		if ((SystemProperties.getInt("persist.asus.autoupload.enable", 0) != 1) && (flag == 0)) //+
            return;
			
		if ((flag == 1) && !checkIfLogFileExistence()) {
            flag = 0;
            return;
        }
		//Joseph_Lin 20140117-

        synchronized (mLock) {
            Log.d(TAG, "auto upload log now !!");
            Log.d(TAG, "service start foreground!!");

            startForeground(1, new Notification());
            mPostTask = new HttpMultipartPostAsyncTask(mContext);
            mPostTask.execute(logDirectory);
        }
    }

    private void uploadLogNow(String filepath){
        synchronized (mLock) {
            Log.d(TAG, "manual upload log now !!");
            Log.d(TAG, "service start foreground!!");

            startForeground(1, new Notification());
            mPostTask = new HttpMultipartPostAsyncTask(mContext);
            mPostTask.execute(filepath);
        }
   }

    private String getLastModifiedFileName(){
        File directory = new File(logDirectory);
        File[] files = directory.listFiles();
        String filename = "";
        if (files != null && files.length >0){
            Arrays.sort(files, new Comparator<File>(){
                public int compare(File f1, File f2)
                {
                    // ignore folders by Joey_Lee
                    if (f1.isDirectory() == true) {
                        return 1;
                    } else if (f2.isDirectory() == true) {
                        return -1;
                    } else {
                        return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
                    }
                }
            });
            filename = files[0].getAbsolutePath();
        }

        return filename;
    }

    private void waitLogFileCreateComplete(){
        String status;
        while(true){
            status = SystemProperties.get("init.svc.logbackup","");
            Log.e(TAG,"pack log service status= " + status);
            if(status != null && status.equals("stopped")){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return;
            }
            try{
                Thread.sleep(1000);
            }catch(Exception e){
                Log.e(TAG,"Exception: " +e.getMessage());
            }
        }
    }

    public void acquirePartialWakeLock(Context context) {
        if (mWakeLock != null) {
            return;
        }
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "okTag");
        mWakeLock.acquire();
    }

    public void releasePartialWakeLock() {
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    public void createLogFile(int reason, String description, String reporter) //manual upload
    {
        synchronized (mLock) {
            Log.d(TAG, "create log file manual!!");
            acquirePartialWakeLock(mContext);
            String filename = "";
            createLogDirIfNotExists();
            createDescriptionFile(reason, description, reporter);

            try {
                Runtime rt = Runtime.getRuntime();
                Process proc = rt.exec("ls -all");
                //start Log creater service
                sendLogCreaterBroadcast(STATUS_LOG_CREATER_PROCESSING);
                saveStatus(STATUS_LOG_CREATER_PROCESSING);
                SystemProperties.set("logbackup","started");
                proc = rt.exec("setprop ctl.start logbackup:" + Integer.toString(reason));
                InputStream is = proc.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    Log.d(TAG,line);
                }
                //waitLogFileCreateComplete();
                //Log creater service finish
                //sendLogCreaterBroadcast(STATUS_LOG_CREATER_COMPLETE);
                //saveStatus(STATUS_LOG_CREATER_COMPLETE);

                //filename = getLastModifiedFileName();

                mHistoryFile.writeDebugLog("generate log manually");
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

    public void createLogFile() //auto upload
    {
        //Harrison, add switch in development settings for auto upload
        if (SystemProperties.getInt("persist.asus.autoupload.enable", 0) != 1)
            return;

        synchronized (mLock) {
            Log.d(TAG, "create log file auto!!");
            acquirePartialWakeLock(mContext);
            createLogDirIfNotExists();
            createDescriptionFile(0, "", "");

            try {
                Runtime rt = Runtime.getRuntime();
                Process proc = rt.exec("ls -all");
                //start Log creater service
                sendLogCreaterBroadcast(STATUS_LOG_CREATER_PROCESSING);
                saveStatus(STATUS_LOG_CREATER_PROCESSING);
                SystemProperties.set("logbackup","started");
                proc = rt.exec("setprop ctl.start logbackup");
                InputStream is = proc.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    Log.d(TAG,line);
                }
                //waitLogFileCreateComplete();
                //Log creater service finish
                //sendLogCreaterBroadcast(STATUS_LOG_CREATER_COMPLETE);
                //saveStatus(STATUS_LOG_CREATER_COMPLETE);

                mHistoryFile.writeDebugLog("generate log automatically");
            } catch (Throwable t) {
               t.printStackTrace();
            }
        }
    }

    public void createDescriptionFile(int reason, String description, String reporter){
        Log.d(TAG, "create description file !!");
        SystemInfo mSystemInfo = new SystemInfo(mContext);
        try{
            File fileName=new File(logDirectory +"/info.txt");
            FileWriter FW = new FileWriter(fileName, false); // false means over write the origin text
            BufferedWriter BW = new BufferedWriter(FW);
            String temp = null;
            BW.write("[Info]");
            BW.newLine();BW.flush();

            BW.write("Model=");
            BW.write(mSystemInfo.mModel);
            BW.newLine();BW.flush();

            BW.write("IMEI=");
            BW.write(mSystemInfo.mIMEINumber);
            BW.newLine();BW.flush();

            BW.write("Firmware=");
            BW.write(mSystemInfo.mFirmwareVersion);
            BW.newLine();BW.flush();

            BW.write("Camera=");
            BW.write(mSystemInfo.mCameraVersion);
            BW.newLine();BW.flush();

            BW.write("Station=");
            BW.write(mSystemInfo.mStationVersion);
            BW.newLine();BW.flush();

            BW.write("Reason=");
            BW.write(Integer.toString(reason));
            BW.newLine();BW.flush();

            BW.write("ReasonDesc=");
            BW.write(description);
            BW.newLine();BW.flush();

            BW.write("Reporter=");
            BW.write(reporter);
            BW.newLine();

            BW.flush();
            BW.close();
        }
        catch(Exception e) {
            System.out.println(e.toString());
        }
    }

    public void sendLogCreaterBroadcast(int status){
        Log.d(TAG,"sendLogCreaterBroadcast: " + status);
        Intent intent=new Intent();
        intent.setAction("file-status");
        intent.putExtra("status",status);
        mContext.sendBroadcast(intent);
    }

    public void saveStatus(int status){
        MainActivity.PROCESSSTATUS = status;
        /*SharedPreferences settings = mContext.getSharedPreferences ("PREF_LOG_CREATER_STATUS", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putInt("STATUS", status);
        PE.commit();*/
    }

    private void saveLastManualTime(long time){
        SharedPreferences settings = mContext.getSharedPreferences ("PREF_LAST_MANUAL_TIME", 0);
        SharedPreferences.Editor PE = settings.edit();
        PE.putLong("TIME", time);
        PE.commit();
    }

    private void createLogDirIfNotExists() {
        try{
            mFileUtils = new FileUtils();
            if(!mFileUtils.mkdir(ASUSDirectory)){
                Log.e(TAG, "Copy temp file to Internal failed");
                try{
                    Runtime.getRuntime().exec("mkdir /sdcard/ASUS");

                }catch(Exception e){
                    e.printStackTrace();
                }
            }
            if(!mFileUtils.mkdir(logDirectory)){
                Log.e(TAG, "Copy temp file to Internal failed");
                try{
                    Runtime.getRuntime().exec("mkdir /sdcard/ASUS/LogUploader");

                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }catch(Exception e){

        }finally{
            mFileUtils = null;
        }
    }

    private void showUploadDialogAndUploadNow() {
        if (SystemProperties.getInt("persist.asus.mupload.enable", 0) != 1)
            return;

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View radioLayout = inflater.inflate(R.layout.uploading_preference_radio_button, null);
        final RadioGroup radioGroup = (RadioGroup) radioLayout.findViewById(R.id.radiogroup);
        String title = "";
        String message = "";

        if(mLoadPreferenceDialog!=null && mLoadPreferenceDialog.isShowing())
            return;

        title = (String) mContext.getResources().getText(R.string.preference_dialog_title);
        message = (String) mContext.getResources().getText(R.string.preference_dialog_summary);

        mLoadPreferenceDialog = new AlertDialog.Builder(LogUploaderService.this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(com.android.internal.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            SharedPreferences firstSend = getSharedPreferences ("FIRST_SEND", 0);
                            firstSend.edit().putBoolean("FIRST_SEND_FLAG", false).commit();

                            int radioButtonID = radioGroup.getCheckedRadioButtonId();
                            SharedPreferences networkpref = PreferenceManager.getDefaultSharedPreferences(mContext);
                            switch (radioButtonID) {
                                case R.id.wifi_only:
                                    networkpref.edit().putBoolean("setting_pref_3gEnable", false).commit();
                                    break;
                                case R.id.any:
                                    networkpref.edit().putBoolean("setting_pref_3gEnable", true).commit();
                                    break;
                            }

                            Log.d(TAG, "mManualUpload = " + mManualUpload);
                            if (mManualUpload) {
                                String filename = getLastModifiedFileName();
//                                uploadLogNow(filename);
                                // because of splitting the upload file, cannot upload only one file by joey_lee@asus.com
                                uploadLogNow(logDirectory);
                            } else {
                                uploadLogNow();
                            }
                        }
                    }
        )
        .setView(radioLayout)
        .create();

        mLoadPreferenceDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        mLoadPreferenceDialog.show();
    }

}
