package com.asus.loguploader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.text.format.Time;
import android.util.Log;
import com.asus.fdclogtool.R;
public class AsdfAbnormalRebootDialog extends Activity {
    private AlertDialog alert = null;
    private AlertDialog.Builder builder = null;
    private boolean isButtonPressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(this.getLocalClassName(), "Enter onCreate()");

        popupAlertDialog(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.d(this.getLocalClassName(), "Enter onSaveInstanceState");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(this.getLocalClassName(), "Enter onConfigurationChanged");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(this.getLocalClassName(), "Enter onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(this.getLocalClassName(), "Enter onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(this.getLocalClassName(), "Enter onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(this.getLocalClassName(), "Enter onStop()");
        Log.d(this.getLocalClassName(), "isButtonPressed = " + isButtonPressed);

        if (isButtonPressed == false) {  //go to onDestroy() if true
            PowerManager mPowerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
            if (mPowerManager != null) {
                boolean isScreenOn = mPowerManager.isScreenOn();
                Log.d(this.getLocalClassName(), "isScreenOn = " + isScreenOn);
                if (isScreenOn == true) {  // press HOME or MENU key
                    writeLog("ASDF: indeterminate shutdown");
                    releaseAlertDialog();
                }
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(this.getLocalClassName(), "Enter onRestart()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(this.getLocalClassName(), "Enter onDestroy()");
    }

    private void popupAlertDialog(Context context) {
        if (alert != null) {
            return;
        }

        builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.asdf_alert);
        builder.setMessage(R.string.asdf_alert_content);
        builder.setNegativeButton(R.string.asdf_no, new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, int item) {
                isButtonPressed = true;
                Log.d(getLocalClassName(), "NegativeButton has been pressed");
                writeLog("ASDF: normal shutdown");
                dialog.cancel();
                releaseAlertDialog();
                finish();
            }
        });
        builder.setPositiveButton(R.string.asdf_yes, new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, int item) {
                isButtonPressed = true;
                Log.d(getLocalClassName(), "PositiveButton has been pressed");
                writeLog("ASDF: abnormal shutdown");
                dialog.cancel();
                releaseAlertDialog();
                finish();
            }
        });
        builder.setCancelable(false);

        alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    private void releaseAlertDialog() {
        if (alert != null) {
            alert.dismiss();
        }
        alert = null;
        builder = null;
    }

    public void writeLog(String content) {
        Log.d(this.getLocalClassName(), "write log to ASUSEvtlog.txt -> " + content);
        try {
            File fileName=new File("/proc/asusevtlog");
            FileWriter FW = new FileWriter(fileName, true);
            BufferedWriter BW = new BufferedWriter(FW);
            BW.write(content);
            BW.newLine();
            BW.flush();
            BW.close();
        }
        catch(Exception e) {
            Log.d(getLocalClassName(), "writeLog() error: " + e.toString());
            System.out.println(e.toString());
        }
    }

}
