package com.asus.loguploader;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemProperties;
import android.util.Log;
import android.widget.Toast;

import com.asus.loguploader.utils.FileUtils;

import android.content.ComponentName;

import java.io.BufferedOutputStream;

public class HttpMultipartPostAsyncTask extends AsyncTask<String, Integer, Boolean>
{
        public static final String TAG = "HttpMultipartPost";

        //uri
        private String postUriTemp1 = "http://10.10.10.2:1085/QualityApi/TrialSystemAPIFormPost.aspx"; //transfer to 172.29.8.185
        private String postUriTemp2 = "http://10.10.10.2:1085/QualityApi/TestPage.aspx";
        private String postUri = "http://172.29.8.185/QualityApi/TrialSystemAPIFormPost.aspx"; //local
        private String postUri2 = "http://60.250.125.18:51900/TrialSystemAPIFormPost.aspx"; //global
        private String postAddressOffical = "usrtrl.asus.com:51900";

        public static int STATUS_TASK_START = 0 ;
        public static int STATUS_TASK_COMPLETE = 1 ;
        public static int STATUS_ERROR_NETWORK = 2 ;

        private Context mContext;

        private NotificationHelper mNotificationHelper;

        private boolean error = false;

        long totalSize;
        FileUtils mFileUtils;

        List<String> uploadFileList = new ArrayList<String>();

        HistoryFile mHistoryFile;
        PowerManager.WakeLock mWakeLock;

        // for measuring the uploading time by joey_lee@asus.com
        private long uploadStartTime = 0L;
        private long uploadEndTime = 0L;
        private Handler mHandler = new Handler();

        public HttpMultipartPostAsyncTask(Context context){

            this.mContext = context;
            mNotificationHelper = new NotificationHelper(context);
            mHistoryFile = new HistoryFile(mContext);
        }

        public void acquirePartialWakeLock(Context context) {
            if (mWakeLock != null) {
                return;
            }
            PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "okTag");
            mWakeLock.acquire();
            Log.d(TAG, "upload wakelock acquire");
        }

        public void releasePartialWakeLock() {
            if (mWakeLock != null) {
                mWakeLock.release();
                mWakeLock = null;
                Log.d(TAG, "upload wakelock release");
            }
        }

        @Override
        protected void onPreExecute()
        {
            //sendUploaderBroadcast(STATUS_TASK_START);
            //saveStatus(STATUS_TASK_START);
            acquirePartialWakeLock(mContext);
        }

        @Override
        protected Boolean doInBackground(String... arg0)
        {
            /*while( (file=getFirstFile(argv[0]))!=null ) {
                UploadAndEraseFile(filepath);
            }*/
            try {
                if (new File(arg0[0]).isDirectory()) {
                    getFilelist(arg0[0]);
                    for(String filepath:uploadFileList) {
                        Log.d(TAG, "candidates of upload file: " + filepath);
                        UploadFile(filepath);
                    }
                } else {
                    Log.d(TAG, "candidate of upload file: " + arg0[0]);
                    UploadFile(arg0[0]);
                }
            } catch(Exception e) {
                Log.d(TAG, "Exception: " + e.getMessage());
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            releasePartialWakeLock();
            mNotificationHelper.completed();
            serviceStopForegroud(mContext);
        }

        private void onErrorHandle(){
            mNotificationHelper.completed();
            //sendUploaderBroadcast(STATUS_ERROR_NETWORK);
            saveStatus(STATUS_ERROR_NETWORK);
        }

        private void getFilelist(String dir){
            File directory = new File(dir);
            File[] files = directory.listFiles();
            Log.d("HttpFileUpload","get file list " + dir);
            if (files != null && files.length >0){
                for (int i = 0; i < files.length; ++i) {
                    uploadFileList.add(files[i].getAbsolutePath());
                }
            }
        }

        private void saveStatus(int status){
            SharedPreferences settings = mContext.getSharedPreferences ("PREF_HTTP_TASK_STATUS", 0);
            SharedPreferences.Editor PE = settings.edit();
            PE.putInt("STATUS", status);
            PE.commit();
        }

        private class filenameFilter implements FilenameFilter {
            private String filename = null;

            public filenameFilter(String filename) {
                this.filename = filename;
            }

            @Override
            public boolean accept(File dir, String filename) {
                try {
                    if (filename.indexOf(this.filename) >= 0) {
                        return true;
                    } else {
                        return false;
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        };

        private void UploadFile(String file){
            String [] token;
            String fileNum = null;
            String fileTotal = null;
            token = file.split("/");
            String filename = token[token.length-1];

            String [] tail;
            tail = filename.split("\\.");
            if (!tail[tail.length-1].equals("gz")) {
                // because of splitting the upload file by joey_lee@asus.com
                if (tail.length < 3 || !tail[tail.length - 3].equals("gz")) {
                    return;
                }
            }

            Log.e(TAG, "UploadFile: " + file);
            if (tail[tail.length-1].equals("gz")) {
                fileNum = fileTotal = "1";
            } else {
                File dir = new File(file.substring(0, file.indexOf(filename)));
                String[] splitFiles = dir.list(new filenameFilter(tail[tail.length - 5]));
                fileTotal = tail[tail.length - 2];
                fileNum = String.valueOf(Integer.parseInt(fileTotal) - splitFiles.length + 1);
                dir = null;
            }
            mNotificationHelper.createNotification(fileNum, fileTotal);
            Log.d(TAG, "uploading " + filename + "  ..." + " (" + fileNum + "/" + fileTotal + ")");
            mHistoryFile.writeDebugLog("uploading " + filename + "  ..." + " (" + fileNum + "/" + fileTotal + ")");

            try {
                // Set your file path here
                FileInputStream fstrm = new FileInputStream(file);

                // Set your server page url (and the file title/description)
                String address = SystemProperties.get("persist.asus.server.addr",postAddressOffical);
                String uri = "http://" + address + "/TrialSystemAPIFormPost.aspx";
                Log.e(TAG, "post uri: " + uri);
                HttpFileUpload hfu = new HttpFileUpload(uri,"1.0",new File(file).getName());

                int retry = 0;  // retry 3 times if upload failed by joey_lee@asus.com
                int response = hfu.Send_Now(fstrm, file);
//                while ((response = hfu.Send_Now(fstrm, file)) != 9) {
                while (response != 9 && response != 0) {
                    retry++;
                    if (retry <= 3) {
                        Log.e(TAG, "fail to upload " + filename + ", response code = " + response + ", retry: " + retry + " after 3 secs");
                        mHistoryFile.writeDebugLog("fail to upload " + filename + ", response code = " + response + ", retry: " + retry + " after 3 secs");
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        fstrm = new FileInputStream(file);
                        response = hfu.Send_Now(fstrm, file);
                    } else {
                        Log.e(TAG, "retry failed...");
                        mHistoryFile.writeDebugLog("retry failed...");
                        break;
                    }
                }

                if(response == 9 || response == 0){// delete files after upload completed
                    mHistoryFile.writeDebugLog("upload " + filename + " done");
                    Log.e(TAG, "delete log file");
                    try{
                        mFileUtils = new FileUtils();
                        if(!mFileUtils.del(file)){
                            Log.e(TAG, "delete file " + filename + " failed");
                        }
                    }catch(InterruptedException e){
                        Log.w(TAG, "copy to internal is interrupted");
                    }finally{
                        mFileUtils = null;
                    }
//                } else if( response == 999) {
                } else {
                    Log.e(TAG, "fail to upload " + filename + ", response code = " + response);
                    mHistoryFile.writeDebugLog("fail to upload " + filename + ", response code = " + response);
                }

            } catch (FileNotFoundException e) {
                // Error: File not found
                Log.i("HttpFileUpload","File not found");
            }
        }

        private void sendUploaderBroadcast(int status){
            Log.d(TAG,"sendUploaderBroadcast: " + status);
            Intent intent=new Intent();
            intent.setAction("uploader-status");
            intent.putExtra("status",status);
            mContext.sendBroadcast(intent);
        }


        public class HttpFileUpload{
            URL connectURL;
            String responseString;
            String Version;
            String iFileName;
            byte[ ] dataToServer;
            FileInputStream fileInputStream = null;
            float byteSending = 0;

            HttpFileUpload(String urlString, String vVersion, String mFileName){
                try{
                    connectURL = new URL(urlString);
                    Version = vVersion;
                    iFileName = mFileName;
                }catch(Exception ex){
                    Log.i("HttpFileUpload","URL Malformatted");
                }
            }

            int Send_Now(FileInputStream fStream, String filePath){
                fileInputStream = fStream;
                byteSending = 0 ;
                return Sending(filePath);
            }

            int Sending(String filePath){
                //String iFileName = "test.png";
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                String Tag="fSnd";
                int response = 0;
                String [] token;
                token = iFileName.split("/");
                final String filename = token[token.length-1];
                try
                {
                    Log.e(Tag,"Starting Http File Sending to URL");

                    // for measuring the uploading time by joey_lee@asus.com
                    try {
                        File temp = new File(filePath);
                        if (temp.exists()) {
                            Log.d(Tag, "upload file size = " + temp.length() + ", about " + temp.length() / 1024 / 1024 + "MB");
                            mHistoryFile.writeDebugLog("upload file size = " + temp.length() + ", about " + temp.length() / 1024 / 1024 + "MB");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    uploadStartTime = System.currentTimeMillis();
                    Log.d(Tag, "uploadStartTime = " + uploadStartTime);
                    mHistoryFile.writeDebugLog("uploadStartTime = " + uploadStartTime);

                    // Open a HTTP connection to the URL
                    HttpURLConnection conn = (HttpURLConnection)connectURL.openConnection();
                    conn.setChunkedStreamingMode(0);

                    // Allow Inputs
                    conn.setDoInput(true);

                    // Allow Outputs
                    conn.setDoOutput(true);

                    // Don't use a cached copy.
                    conn.setUseCaches(false);

                    // Use a post method.
                    conn.setRequestMethod("POST");

                    conn.setRequestProperty("Connection", "Keep-Alive");

                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);


                    DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(conn.getOutputStream()));

                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    dos.writeBytes("Content-Disposition: form-data; name=\"version\""+ lineEnd);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(Version);
                    dos.writeBytes(lineEnd);

                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    dos.writeBytes("Content-Disposition: form-data; name=\"FileName\""+ lineEnd);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(iFileName);
                    dos.writeBytes(lineEnd);

                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    dos.writeBytes("Content-Disposition: form-data; name=\"file1\";filename=\"" + iFileName +"\"" + lineEnd);
                    dos.writeBytes("Content-Type: file \r\n");
                    dos.writeBytes(lineEnd);

                    Log.e(Tag,"Headers are written");

                    // create a buffer of maximum size
                    int bytesAvailable = fileInputStream.available();
                    float totoalBytes = bytesAvailable;

                    int maxBufferSize = 1024;
                    int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    byte[ ] buffer = new byte[bufferSize];

                    // read file and write it into form...
                    int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    publishProgress((int)(byteSending/totoalBytes*100)); //notification progress
                    int sendProgress = 0;

                    while (bytesRead > 0)
                    {
                        byteSending+=bytesRead;
                        dos.write(buffer, 0, bufferSize);
                        if((int)(byteSending/totoalBytes*100) > sendProgress) {
                            sendProgress = (int)(byteSending/totoalBytes*100);
                            publishProgress(sendProgress); //notification progress
                        }
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable,maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0,bufferSize);
                    }
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    // close streams
                    fileInputStream.close();
                    fileInputStream = null;

                    dos.flush();

                    int responseCode = conn.getResponseCode();
                    Log.e(Tag, "File Sent, ResponseCode: " + String.valueOf(responseCode));
                    Log.e(Tag, "File Sent, ResponseMessage: " + conn.getResponseMessage());
                    Log.e(Tag,"connectURL = "+ connectURL.toString());

                    InputStream is;
                    if (responseCode == 200) {
                        is = conn.getInputStream();
                    } else {
                        is = conn.getErrorStream();
                    }
//                    InputStream is = conn.getInputStream();

                    // retrieve the response from server
                    int ch;

                    StringBuffer b =new StringBuffer();
                    while( ( ch = is.read() ) != -1 ){ b.append( (char)ch ); }
                    String s=b.toString();
                    if (responseCode == 200) {
                        response = Integer.valueOf(s);
                        if (response == 9 || response == 0) {  // 0 means upload success but write database failed
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(mContext, "upload " + filename + " done", Toast.LENGTH_LONG).show();
                                }
                            });
                            onErrorHandle();
                        }
                    } else {
                        response = 999;
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(mContext, "fail to upload " + filename + ", 500: internal server error", Toast.LENGTH_LONG).show();
                            }
                        });
                        onErrorHandle();
                    }
                    Log.i("Response",s);
                    dos.close();

                    conn.disconnect();

                    //sendUploaderBroadcast(STATUS_TASK_COMPLETE);
                    //saveStatus(STATUS_TASK_COMPLETE);
                    mNotificationHelper.completed();

                }
                catch (final MalformedURLException ex)
                {
                    response = 999;
                    error = true;
                    Log.e(Tag, "URL error: " + ex.getMessage(), ex);
                    mHistoryFile.writeDebugLog("fail to upload " + filename + ", URL error:" + ex.getMessage());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, "fail to upload " + filename + ", URL error:" + ex.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                    onErrorHandle();
                }
                catch (final IOException ioe)
                {
                    response = 999;
                    error = true;
                    Log.e(Tag, "IO error: " + ioe.getMessage(), ioe);
                    mHistoryFile.writeDebugLog("fail to upload " + filename + ", IO error:" + ioe.getMessage());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, "fail to upload " + filename + ", IO error:" + ioe.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                    onErrorHandle();
                }

                try {
                    if (fileInputStream != null) {
                        fileInputStream.close();
                        fileInputStream = null;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(Tag, "fileInputStream.close() error: " + e.toString());
                }

                if (error == false) {
                    // for measuring the uploading time by joey_lee@asus.com
                    uploadEndTime = System.currentTimeMillis();
                    Log.d(Tag, "uploadEndTime = " + uploadEndTime);
                    mHistoryFile.writeDebugLog("uploadEndTime = " + uploadEndTime);
                    long uploadTotalTime = uploadEndTime - uploadStartTime;
                    Log.d(TAG, "total upload time = " + uploadTotalTime + ", about " + (float)uploadTotalTime / 1000.0 + "secs");
                    mHistoryFile.writeDebugLog("total upload time = " + uploadTotalTime + ", about " + (float)uploadTotalTime / 1000.0 + "secs");
                } else {
                    Log.d(TAG, "don't need to count total upload time 'cause upload failed");
                    mHistoryFile.writeDebugLog("don't need to count total upload time 'cause upload failed");
                }

                return response;
            }

        }

        protected void onProgressUpdate(Integer... progress) {

        //This method runs on the UI thread, it receives progress updates
        //from the background thread and publishes them to the status bar

        mNotificationHelper.progressUpdate(progress[0]);

        }

        private void serviceStopForegroud(Context context){
            Intent in = new Intent("stop-foreground");
            in.setComponent(new ComponentName("com.asus.loguploader", "com.asus.loguploader.LogUploaderService"));
            context.startService(in);
        }

}
