package com.asus.loguploader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import com.asus.fdclogtool.R;
public class LogActivity extends Activity {
    public static final String TAG = "LogUploader";
    private static final int mLineRestriction = 1000;
    private BufferedReader mBufferReader;
    private LineNumberReader mLineNumberReader;
    private TextView mTextView;
    private ScrollView mScrollView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log);
        mTextView = (TextView) findViewById(R.id.log_content);
        mScrollView = (ScrollView) findViewById(R.id.log_scroll);
    }

    public void onResume() {
        super.onResume();
        String dir = getDataDirectory();
        File readerFile = new File(dir + "/log.txt");
        try {

            //mBufferReader = new BufferedReader(new FileReader(readerFile));

            mLineNumberReader = new LineNumberReader(new FileReader(readerFile));
            mLineNumberReader.skip(readerFile.length());
            int lineNumber = mLineNumberReader.getLineNumber();
            mLineNumberReader.close();
            Log.e(TAG, "total line number = " + lineNumber);

            mLineNumberReader = new LineNumberReader(new FileReader(readerFile));

            int startLine = 1;
            if(lineNumber > mLineRestriction){
                startLine = lineNumber - mLineRestriction;
            }

            String tempString = null;

            while ((tempString = mLineNumberReader.readLine()) != null) {
                if (mLineNumberReader.getLineNumber() > startLine){
                    mTextView.append(tempString);
                    mTextView.append("\n");
                }
            }
            mLineNumberReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mBufferReader != null) {
                try {
                    mBufferReader.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }

        scrollToBottom(mScrollView,mTextView);
    }

    public void onPause(){
        super.onPause();
        mTextView.setText("");
    }

    private String getDataDirectory(){
        PackageManager m = this.getPackageManager();
        String s = this.getPackageName();
        try {
            PackageInfo p = m.getPackageInfo(s, 0);
            s = p.applicationInfo.dataDir;
        } catch (NameNotFoundException e) {
            Log.d(TAG, "Error Package name not found ", e);
        }

        return s;
    }

    public static void scrollToBottom(final View scroll, final View inner) {
        Handler mHandler = new Handler();
        mHandler.post(new Runnable() {
            public void run() {
                if (scroll == null || inner == null) {
                    return;
                }

                int offset = inner.getMeasuredHeight() - scroll.getHeight();
                if (offset < 0) {
                    offset = 0;
                }

                scroll.scrollTo(0, offset);
            }
        });
    }
}