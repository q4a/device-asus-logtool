package com.asus.loguploader;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import com.asus.fdclogtool.R;
public class MyTabActivity extends TabActivity {
    /** Called when the activity is first created. */
    private static final String TAG= "MyTabActivity";
//  private ScreenshotObserver mScreenshotFileObserver;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_host);
        TabHost tabHost = getTabHost();

        // Tab for UI
        TabSpec uispec = tabHost.newTabSpec("UPLOAD");
        // setting Title and Icon for the Tab
        uispec.setIndicator("UPLOAD");
        Intent uiIntent = new Intent(this, MainActivity.class);
        uiIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        uispec.setContent(uiIntent);

        // Tab for Log
        TabSpec logspec = tabHost.newTabSpec("HISTORY");
        // setting Title and Icon for the Tab
        logspec.setIndicator("HISTORY");
        Intent logIntent = new Intent(this, LogActivity.class);
        logspec.setContent(logIntent);


        // Adding all TabSpec to TabHost
        tabHost.addTab(uispec); // Adding UI tab
        tabHost.addTab(logspec); // Adding log tab

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.upload_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
        case R.id.menu_settings:
            startActivity(new Intent(this, Settings.class));
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}