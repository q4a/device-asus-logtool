package com.asus.loguploader;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.os.SystemProperties;

public class AsdfAbnormalRebootReceiver extends BroadcastReceiver {
    private static final String LOG = "AsdfAbnormalRebootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(LOG, "onReceive(), context: " + context.toString() + ", intent: " + intent.toString());

        String type = SystemProperties.get("ro.build.type", "unknown");
        int mUpload = SystemProperties.getInt("persist.asus.mupload.enable", 0);
        Log.d(LOG, "ro.build.type = " + type);
        Log.d(LOG, "persist.asus.mupload.enable = " + mUpload);
        if (type.equals("user") && mUpload != 1)
        {
            Log.d(LOG, "user version without AsusLogTool, return");
            return;
        }

        Intent dialogIntent = new Intent();
        dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        dialogIntent.setClass(context, AsdfAbnormalRebootDialog.class);
        context.startActivity(dialogIntent);
    }

}
