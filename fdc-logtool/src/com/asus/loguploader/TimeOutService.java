package com.asus.loguploader;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

public class TimeOutService extends Service {
    private Context mContext;
    private final String TAG = "TimeOutService";
    private TimeOutServiceBinder mLocBin = new TimeOutServiceBinder();
    private boolean mRunning;

    public class TimeOutServiceBinder extends Binder {
        TimeOutService getService() {
            return TimeOutService.this;
        }
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate()");
        super.onCreate();
        mContext = this;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("stopTimer");
        registerReceiver(mTimerReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        super.onDestroy();
        unregisterReceiver(mTimerReceiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        if(intent == null)
            return START_STICKY;

        String action = intent.getAction();
        if("startTimer".equals(action)) {
            new Thread() {
                @Override
                public void run() {
                    Log.d(TAG, "Start Timer");
                    long time = SystemClock.elapsedRealtime();
                    mRunning = true;
                    while(mRunning) {
                        long now = SystemClock.elapsedRealtime();
                        long pass = now - time;
                        if(pass > 60000) {
                            Intent mIntent = new Intent();
                            mIntent.setAction("com.asus.packlogs.completed");
                            mContext.sendBroadcast(mIntent);
                            break;
                        }
                    }
                }
            }.start();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mLocBin;
    }

    private BroadcastReceiver mTimerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Stop Timer");
            mRunning = false;
        }
    };
}