package com.asus.loguploader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import com.asus.fdclogtool.R;
public class MainActivity extends Activity {
    public static final String TAG = "LogUploader";

    private Context context;

    private Button send_button;
    private Button cancel_button;
    private Button mReasonPicker;
    private EditText reason_desc;
    private EditText reporter;

    LogUploaderService mService;

    AlertDialog mLangDlg = null;
    private ProgressDialog mProgressDialog;

    ArrayAdapter<CharSequence> bug_adapter;

    private static int REASON_AUTO_UPLOAD  = 0;
    private static int REASON_UNKNOWN  = 1;
    private static int REASON_USER_DEFINE = 2;
    private static int REASON_UI_LAG  = 101;
    private static int REASON_UI_FORCE_CLOSE = 102;
    private static int REASON_UI_DDS_FAIL = 103;
    private static int REASON_AUTO_REBOOT = 211;
    private static int REASON_DEVICE_FREEZE  = 212;
    private static int REASON_POWER_DRAINS_QUICKLY  = 221;
    private static int REASON_POWER_OVER_HEAT = 222;
    private static int REASON_DEVICE_TOUCH_FAIL = 231;
    private static int REASON_CALL_CANT_MAKE_CALLS = 301;
    private static int REASON_DROP_CALLS  = 302;
    private static int REASON_CALL_NO_RADIO  = 303;
    private static int REASON_CALL_NO_AUDIO = 304;
    private static int REASON_CALL_POOR_AUDIO_QUALITY  = 305;
    private static int REASON_NETWORK_CANT_CONNECT_DATA = 321;
    private static int REASON_NETWORK_CANT_CONNECT_WIFI = 322;

    private int [] reasons = {REASON_UI_LAG, REASON_UI_FORCE_CLOSE, REASON_UI_DDS_FAIL,
            REASON_AUTO_REBOOT, REASON_DEVICE_FREEZE,
            REASON_POWER_DRAINS_QUICKLY, REASON_POWER_OVER_HEAT,
            REASON_DEVICE_TOUCH_FAIL,
            REASON_CALL_CANT_MAKE_CALLS, REASON_DROP_CALLS, REASON_CALL_NO_RADIO, REASON_CALL_NO_AUDIO, REASON_CALL_POOR_AUDIO_QUALITY,
            REASON_NETWORK_CANT_CONNECT_DATA,  REASON_NETWORK_CANT_CONNECT_WIFI,
            REASON_USER_DEFINE};

    private int mReason = REASON_UI_LAG;
    private String mDescription = "";
    private String mReporter = "";

    private boolean mIsLogUploaderReceiver = false;
    private boolean mIsLogCreaterrReceiver = false;

    private boolean mButtonSend = true;
    private boolean mManual = false;

//    private int mReasonItemNumber = 15;
    private int mReasonItemNumber = reasons.length - 1;  // modified by joey_lee@asus.com

    private static final int DIALOG_NETWORK =0;
    private static final int DIALOG_MANUAL_ALLOWANCE = 1;
    private static final int DIALOG_COMMENT_EMPTY = 2;
    private static final int DIALOG_FIRST_START = 3;

    public static int PROCESSSTATUS = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_main);
        if (savedInstanceState != null ) {
            mReasonItemNumber = savedInstanceState.getInt("reason");
        }

        mReason = reasons[mReasonItemNumber];
        context = this;

        bug_adapter = ArrayAdapter.createFromResource(this,
                R.array.bug_list, android.R.layout.select_dialog_singlechoice);

        mReasonPicker = (Button) findViewById(R.id.reasonpicker);

        send_button = (Button)findViewById(R.id.send_button);
        send_button.setOnClickListener(send_button_listener);
        cancel_button = (Button)findViewById(R.id.cancel_button);
        cancel_button.setOnClickListener(cancel_button_listener);

        reason_desc = (EditText)findViewById(R.id.other_field);
        reporter = (EditText)findViewById(R.id.edit_reporter);

        mReasonPicker = (Button) findViewById(R.id.reasonpicker);
        mReasonPicker.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                PickertBtnClick(v);
            }
        });

        InitPopDialog();
        doBindService();
        registerLogUploaderReceiver();
        getPref();

        /*SharedPreferences firstStart = getSharedPreferences ("FIRST_START", 0);
        boolean flag = firstStart.getBoolean("FIRST_START_FLAG", true);
        if(flag)
            CreateAlertDialog(DIALOG_FIRST_START);*/
    }

    public void onResume(){
        Log.d(TAG,"onResume");
        super.onResume();
        doBindService();
        registerLogUploaderReceiver();
        registerLogCreaterReceiver();

        updateButton();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG,"onDestroy");
        super.onDestroy();
        unbindService(mConnection);
        unregisterLogUploaderReceiver();
        unregisterLogCreaterReceiver();

        SharedPreferences firstStart = getSharedPreferences("FIRST_START", 0);
        firstStart.edit().putBoolean("FIRST_START_FLAG", false).commit();
    }

    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("reason", mReasonItemNumber);
    }

    void InitPopDialog() {
        String langLable = bug_adapter.getItem(mReasonItemNumber).toString();
        mReasonPicker.setText(langLable);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Problems");
        builder.setSingleChoiceItems(bug_adapter, mReasonItemNumber , new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                mReasonItemNumber = item;
                mReason = reasons[item];
                String bugItem = bug_adapter.getItem(item).toString();
                mReasonPicker.setText(bugItem);
                mLangDlg.dismiss();
            }
        });
        mLangDlg = builder.create();
    }

    private void PickertBtnClick(View v) {
        mLangDlg.show();
    }

    private void updateButton() {
        Log.d(TAG,"update button");
        //SharedPreferences task_status = getApplicationContext().getSharedPreferences ("PREF_LOG_CREATER_STATUS", 0);
        //int status = task_status.getInt("STATUS", LogUploaderService.STATUS_LOG_CREATER_COMPLETE);
        int status = PROCESSSTATUS;

        if(status == LogUploaderService.STATUS_LOG_CREATER_PROCESSING) {
            send_button.setClickable(false);
            send_button.setText(getString(R.string.processing_bottom));
        } else if (status == LogUploaderService.STATUS_LOG_CREATER_COMPLETE) {
            send_button.setClickable(true);
            send_button.setText(getString(R.string.upload_bottom));
        }
    }

    private OnClickListener send_button_listener = new OnClickListener() {
        public void onClick(View v) {
            SharedPreferences task_status = getApplicationContext().getSharedPreferences ("PREF_LAST_MANUAL_TIME", 0);
            long time = task_status.getLong("TIME", 0);
            long now = SystemClock.elapsedRealtime();
            long pass = now - time;

            //if(pass > 60000){ // pass more than 60s since last manual
                mManual = true;
                //send_button.setClickable(false);
                //send_button.setText(getString(R.string.processing_bottom));
                mDescription = reason_desc.getText().toString();
                mReporter = reporter.getText().toString();

                if(mReasonItemNumber == 15 && mDescription.trim().equals("")) {
                    CreateAlertDialog(DIALOG_COMMENT_EMPTY);
                } else {
                    Intent myintent = new Intent(getApplicationContext(), LogUploaderService.class);
                    myintent.setAction("manual-action");
                    myintent.putExtra("reason", mReason);
                    myintent.putExtra("description", mDescription);
                    myintent.putExtra("reporter", mReporter);
                    startService(myintent);

                    Intent mTimeOutIntent = new Intent(getApplicationContext(), TimeOutService.class);
                    mTimeOutIntent.setAction("startTimer");
                    startService(mTimeOutIntent);

                    clearData();
                }
            //}else{
            //  CreateAlertDialog(DIALOG_MANUAL_ALLOWANCE);
            //}
            setPref();
        }
    };

    private OnClickListener cancel_button_listener = new OnClickListener() {
        public void onClick(View v) {
            finish();
        }
    };

    private void clearData() {
        reason_desc.setText("");
        mDescription = "";
        mReasonItemNumber = 15;
        InitPopDialog();
    }

    private void setPref() {
        SharedPreferences settings = getSharedPreferences("USER_PREF", 0);
        settings.edit()
            .putString("pref_reporter", reporter.getText().toString())
            .commit();
    }

    private void getPref() {
        SharedPreferences settings = getSharedPreferences("USER_PREF", 0);
        String name = settings.getString("pref_reporter", "");

        reporter.setText(name);
    }

    private void registerLogUploaderReceiver() {
        if (!mIsLogUploaderReceiver) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("uploader-status");
            registerReceiver(mLogUploaderReceiver, intentFilter);
            mIsLogUploaderReceiver = true;
        }
    }

    private void unregisterLogUploaderReceiver() {
        if (mIsLogUploaderReceiver) {
            unregisterReceiver(mLogUploaderReceiver);
            mIsLogUploaderReceiver = false;
        }
    }

    private void registerLogCreaterReceiver() {
        if (!mIsLogCreaterrReceiver) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("file-status");
            registerReceiver(mLogCreaterReceiver, intentFilter);
            mIsLogCreaterrReceiver = true;
        }
    }

    private void unregisterLogCreaterReceiver() {
        if (mIsLogCreaterrReceiver) {
            unregisterReceiver(mLogCreaterReceiver);
            mIsLogCreaterrReceiver = false;
        }
    }

    private BroadcastReceiver mLogUploaderReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra("status", 1);
            Log.d(TAG,"getUploaderBroadcast: " + status);
            if(status == HttpMultipartPostAsyncTask.STATUS_TASK_START) {
                send_button.setClickable(false);
                send_button.setText(getString(R.string.processing_bottom));
            } else if (status == HttpMultipartPostAsyncTask.STATUS_TASK_COMPLETE) {
                send_button.setClickable(true);
                send_button.setText(getString(R.string.upload_bottom));
            } else if (status == HttpMultipartPostAsyncTask.STATUS_ERROR_NETWORK) {
                //send_button.setClickable(true);
                //send_button.setText(getString(R.string.upload_bottom));
                if (mManual) {
                    CreateAlertDialog(DIALOG_NETWORK);
                    mManual = false;
                }
            }
        }
    };

    private BroadcastReceiver mLogCreaterReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra("status", 1);
            Log.d(TAG,"getUploaderBroadcast: " + status);
            if(status == LogUploaderService.STATUS_LOG_CREATER_PROCESSING) {
                ShowProcessingDialog();
                send_button.setClickable(false);
                send_button.setText(getString(R.string.processing_bottom));
            } else if (status == LogUploaderService.STATUS_LOG_CREATER_COMPLETE) {
                send_button.setClickable(true);
                send_button.setText(getString(R.string.upload_bottom));
                DismissProcessingDialog();
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder) {
            mService = ((LogUploaderService.LogUploaderServiceBinder) binder).getService();
        }
        public void onServiceDisconnected(ComponentName className) {
            mService = null;
        }
    };

    void doBindService() {
        bindService(new Intent(this, LogUploaderService.class), mConnection,
            Context.BIND_AUTO_CREATE);
    }

    private void CreateAlertDialog(int error) {
        Log.d(TAG,"CreateAlertDialog: " + error);
        String title = "";
        String message = "";
        switch(error) {
            case DIALOG_NETWORK:
                title = (String) this.getResources().getText(R.string.error_network_dialog_title);
                message =  (String) this.getResources().getText(R.string.error_network_dialog_message);
                break;
            case DIALOG_MANUAL_ALLOWANCE:
                title = (String) this.getResources().getText(R.string.error_manual_allowance_dialog_title);
                message =  (String) this.getResources().getText(R.string.error_manual_allowance_dialog_message);
                break;
            case DIALOG_COMMENT_EMPTY:
                title = (String) this.getResources().getText(R.string.error_comment_empty_title);
                message =  (String) this.getResources().getText(R.string.error_comment_empty_message);
                break;
            case DIALOG_FIRST_START:
                title = (String) this.getResources().getText(R.string.first_start_title);
                message =  (String) this.getResources().getText(R.string.first_start_message);
                break;
            default:
                break;
        }
        AlertDialog dialog = new AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(com.android.internal.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }
        )
        .create();
        dialog.show();
    }

    private void ShowProcessingDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(this.getResources().getText(R.string.processing_bottom));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    private void DismissProcessingDialog() {
        // Harrison_Chen: fix null pointer exception when show processing dialog
        // add by Joey_Lee
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        Intent intent = new Intent();
        intent.setAction("stopTimer");
        context.sendBroadcast(intent);
        finish();
    }
}