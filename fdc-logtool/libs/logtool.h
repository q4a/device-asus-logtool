#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>

#define KEY_SAVE_DIR 			"persist.asuslog.savedir"
#define DUMP_ENABLE 			"persist.asuslog.dump.enable"
#define KEY_ENABLE_TCPDUMP 		"persist.asuslog.tcpdump.enable"
#define KEY_ENABLE_MAIN 		"persist.asuslog.main.enable"
#define KEY_ENABLE_KERNEL 		"persist.asuslog.kernel.enable"
#define KEY_ENABLE_RADIO 		"persist.asuslog.radio.enable"
#define KEY_ENABLE_EVENT 		"persist.asuslog.events.enable"
//#define KEY_ENABLE_COREDUMP 	"persist.asuslog.modem1.enable"
#define KEY_ENABLE_MODEM1 		"persist.asuslog.modem1.enable"
#define KEY_ENABLE_MODEM2 		"persist.asuslog.modem2.enable"

#define KEY_ENABLE_FACTORY_MODEM1 	"sys.asuslog.modem1.enable"
#define KEY_ENABLE_FACTORY_MODEM2 	"sys.asuslog.modem2.enable"
#define KEY_FDCMDM_REMINDER1	"persist.asuslog.mdm1_reminder"
#define KEY_FDCMDM_REMINDER2 	"persist.asuslog.mdm2_reminder"

#define KEY_ENABLE_MODEM1 		"persist.asuslog.modem1.enable"
#define KEY_ENABLE_MODEM2 		"persist.asuslog.modem2.enable"
#define KEY_ENABLE_CRASHLOG 	"persist.asuslog.fdc_crashlog"

#define KEY_COMBINE_CONFIG 		"persist.asuslog.combine.config"
#define KEY_TEMP_DATE 			"sys.asuslog.date.temp"
#define KEY_TEMP_SDATE			"sys.asuslog.fdate.temp"
#define KEY_DATE_PREPARE		"sys.asuslog.date.prepare"

#define KEY_BOOTMOUNT_STATE		"persist.asuslog.mount.state"
#define DUMP_ENABLE 			"persist.asuslog.dump.enable"
#define MTS_ROTATE_NUM			"persist.service.mts.rotate_num"

#define KEY_DATE_FOLDER	 		"persist.asuslog.dump.date"
#define KEY_INDEX_FOLDER		"persist.asuslog.dump.index"
#define KEY_FOLDER				"persist.asuslog.dump.folder"
#define KEY_ROTATE_TOTAL 		"persist.asuslog.rotate.num"
#define KEY_BUILD_NOAPP 		"persist.asuslog.no_app"
#define KEY_IMAGE_TYPE			 "ro.build.type"
#define LOG_PATH_CHANGE 		"LOG_PATH_CHANGE"
#define FILE_NAME_TCPDUMP 		"tcpdump"
#define FILE_NAME_BPLOG 		"bplog"
#define DIR_NAME_CRASH	    	 "crashlog"

#define KEY_KERNEL_LOG_SIZE		"persist.asuslog.kernellog.size"
#define KEY_MAIN_LOG_SIZE		"persist.asuslog.mainlog.size"
#define KEY_RADIO_LOG_SIZE		"persist.asuslog.radiolog.size"
#define KEY_EVENT_LOG_SIZE		"persist.asuslog.eventlog.size"
#define KEY_COMBINE_LOG_SIZE	"persist.asuslog.combinelog.size"

#define SYSTEM_POS     "/system/bin/"
#define APP_POS 	   "/data/asuslog/files/"
#define FDC_CRASHLOG   "fdc_crashlog"
#define SYSTEM_CRASH   SYSTEM_POS FDC_CRASHLOG
#define APP_CRASH  	   APP_POS FDC_CRASHLOG

#define DIR_NAME_TCPDUMP			"TcpDump"
#define DIR_ROOT 					"/data/media/0/Asuslog/"
#define LOGCAT_KERNEL_PATH 			"/dev/log/kernel"
#define DIR_ROOT_NO_ZYGOTE 			"/data/Asuslog/"
#define DIR_DATA    				"/data"
#define DIR_SDCARD					"/sdcard"
#define DIR_MICRO_SDCRAR	    	"/Removable"
#define MAX_LOG_LEN		(5*1024+1)
#define DIR_MICRO_WAIT_MOUNT_DIR 	"/data/Asuslog/"
#define INDEX_FILE_PATH             "/data/asuslog/logtool_index"
#define INDEX_DIR_PATH              "/data/asuslog/"
#define MAX_LOGFILE_KBSIZE	8000
#define BUF_SIZE 256
#define BUF_TIME_SIZE PROPERTY_VALUE_MAX
#define PATH_SIZE 256
#define DISABLE 		"0"
#define NO_EXIST 		"-2"
#define ENABLE 			"1"
#define ERROR_BOOTSTRAP_UNMOUNT 65280
#define LOG_DEBUG true
#define log SLOGI
#ifdef __cplusplus
extern "C" {
#endif
	void getDate(char* pszBuf,int len);
	//char* appendCmd(char * prefix,char *posfix);
	int execCmd( char * prefix, char *posfix);
	void getLogRootPath(char* pszBuf);
	void setLogRootPath(char * path);
	bool checkEnble(char*value);
	void createLogData(char * storePath,char * dirPath,char* date,char * filename);
	int openfile (const char *pathname);
	long long getFileSize(int fd);
	int rotateLogs(char *filePath,char * fileNamePrevix,int fd,bool bUpdateMtp);
	int closelogfd(int fd,char * dirPath,bool updateMtp);
	int createDir( char* dirpath,bool bchangePermission,bool updateMtp);
	void broadcastMountDir(char* dirpath);
	void broadcastMountPath(char* filepath);
	bool isMtpUpdate(char * path);
	bool isExternelStrorage(char * path);
	bool isMicroStrorage(char * path);
	bool isBootMountEnable();
	void setBootMountEnable(bool enable);
	bool isSaveMicroSD();
	bool isDataStrorage(char * path);
	void onHandelLogPath(bool waitMircoSDBootMount,char * storePath,char * dirPath,char * fileNamePrevix );
	void waitMount(bool saveMicroSD,char* path);
	bool checkMemorySafe(char* path);
	void lockUnMount(int returnCode,char * path);
	void createDirWaitMount(char *path);
	void getDumpDate(char * date);
	void getIndex(char * index);
	void setIndex(char *index);
	void getComDate(char* comDate);
	long getuptime();
	int getBuildSDK();
	bool isFileExist(char * path);
	bool isDirExist(char* dirpath);
	bool isModem1Enable();
	bool isModem2Enable();
	bool isModem1MdmReminder();
	bool isModem2MdmReminder();
	bool isCrashLogSupport();
	bool isUserBuild();

#ifdef __cplusplus
}
#endif
