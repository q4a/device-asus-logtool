#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/un.h>
#include <cutils/sockets.h>
#include <pthread.h>
#include <android/log.h>

#include <cutils/log.h>
#include <cutils/sockets.h>

#define LOG_TAG "toolconnect"
#define SOCKET_NAME "toolconnect"

void abnormalExit(int client_fd);

void abnormalExit(int client_fd) {
	close(client_fd);
	exit(1);
}

void* Runnable(void * arg) {
	int client_fd = *((int *) arg);
	char app_command[512] = { 0 };
	char szReturn[] = { "success" };
	int numbytes;

	if ((numbytes = recv(client_fd, app_command, sizeof app_command, 0))
			== -1) {
		SLOGE("errnoid=%d ,recv error %s", errno, strerror(errno));
		abnormalExit(client_fd);
	}

	if (numbytes == 0) { //app abnormal close or app close_socket ,or other
		abnormalExit(client_fd);
	}

	int reult = system(app_command);
	SLOGI("cmd=%s,result=%d", app_command, reult);
	send(client_fd, szReturn, strlen(szReturn), 0);
	close(client_fd);
	return 0;
}

int main() {
	char buff[200] = { 0 };

	int numbytes;
	struct sockaddr_un peeraddr;
	socklen_t socklen = sizeof(peeraddr);
	//獲取init.rc中配置的名為 SOCKET_NAME 的socket; path at /dev/socket
	int fd = android_get_control_socket(SOCKET_NAME);
	if (fd < 0) {
		SLOGE(
				"Failed to get socket '" SOCKET_NAME "' errno:%s", strerror(errno));
		exit(-1);
	}
	//開始監聽
	int connect_number = 5;
	int ret = listen(fd, connect_number);
	SLOGI("Listen result %d", ret);
	if (ret < 0) {
		SLOGE("listen error %s", strerror(errno));
		exit(-1);
	}

	//等待Socket客戶端發啟連接請求
	int client_fd = 0;
	while ((client_fd = accept(fd, (struct sockaddr *) &peeraddr, &socklen))
			!= -1) {
		//SLOGI("Accept_fd=%d , path=%s" ,client_fd,peeraddr.sun_path);
		if (client_fd < 0) {
			SLOGE("accept error %s", strerror(errno));
			exit(-1);
		}
		pthread_t id;
		int ret = pthread_create(&id, NULL, Runnable, &client_fd);
		if (ret != 0) {
			printf("Create pthread error!\n");
		}

	}
	SLOGI("send finish");
	SLOGI("socket service end");
	return 0;
}

