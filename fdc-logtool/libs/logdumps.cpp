#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include "logtool.h"

#define LOG_TAG "LogTool"
char gszLogRootDir[BUF_SIZE]={0};
char gszLogDir[BUF_SIZE]={0};
void dumpLog(char* data);
#define MAIN_LOG 1
#define KERNEL_LOG 2
#define RADIO_LOG 3
#define EVENT_LOG 4
#define COMBINE_LOG 5
#define TCP_DUMP 6

#define SDK_4_3 18


#define LOG_COMBINE_MAIN 	1
#define LOG_COMBINE_SYSTEM 	2
#define LOG_COMBINE_KERNEL 	4
#define LOG_COMBINE_RADIO  8
#define LOG_COMBINE_EVENTS 16
#define DEFAULT_LOG_SIZE 200000
#define LOG_FILE_DIR    "/dev/log/"
int execLogCmd(char * logOpt, char *cmd,char *storePath,char * gszLogDir,int size);

int main(int argc, char **argv)
{
	char szValue[PROPERTY_VALUE_MAX]={0};
	char szDebug[255]={0};
	char szDate[BUF_TIME_SIZE]={0};
	char szPersistTemp[BUF_TIME_SIZE]={0};
	char szSysTemp[BUF_TIME_SIZE]={0};
	char szIndex[16]={0};
	char szComDate[PROPERTY_VALUE_MAX]={0};
	char szMediaPrepare[PROPERTY_VALUE_MAX]={0};
	char szDatePrepare[PROPERTY_VALUE_MAX]={0};
	char szLogtoolVersion[PROPERTY_VALUE_MAX]={0};
	SLOGI("Woody: argv[1]: %s",argv[1]);

	if(argc==2 && 0 == strcmp(argv[1], "date"))
	{
		while(1){
		//for(int i=0;i<99;i++){

			getDate(szDate,BUF_TIME_SIZE);
			property_set(KEY_DATE_FOLDER, szDate);
			SLOGI("szDate: %s",szDate);
			property_set(KEY_TEMP_SDATE,szDate);
			property_get(KEY_TEMP_SDATE,szPersistTemp,"");
			SLOGI("szPersistTemp: %s",szPersistTemp);
			property_set(KEY_TEMP_DATE,szDate);
			property_get(KEY_TEMP_DATE,szSysTemp,"");
			SLOGI("szSysTempmodify: %s",szSysTemp);

			int result=0;
			if(isDirExist(INDEX_DIR_PATH)==false){
				SLOGI("no /data/asuslog");
				createDirWaitMount(INDEX_DIR_PATH);
			}

			if(isFileExist(INDEX_FILE_PATH)==false){
				SLOGI("no index file");
				result=execCmd("touch", INDEX_FILE_PATH);
				execCmd("chmod -R 777", INDEX_FILE_PATH);
			}
			
			if(result==0 && strcmp(szPersistTemp,szDate)==0 && strcmp(szSysTemp,szDate)==0){
				//property_get("persist.asuslog.logtool.version",szLogtoolVersion,"");
				//if(strcmp("app",szLogtoolVersion) == 0 || strcmp("server",szLogtoolVersion) == 0){
					SLOGI("Woody: getIndex");

					getIndex(szIndex);
					if(strlen(szIndex) == 0)
					{
						sprintf(szIndex, "001");
					}
					SLOGI("szIndex: %s",szIndex);

					//SLOGI(szDate);
					property_set(KEY_DATE_FOLDER, szDate);
					int ind = atoi(szIndex);
					char newIndex[16] = {0};
					sprintf(newIndex, "%03d", ++ind);
					property_set(KEY_INDEX_FOLDER, newIndex);
					setIndex(newIndex);

					sprintf(szComDate, "%.3s_%s", szIndex, szDate);
					SLOGI("szComDate: %s",szComDate);

					property_set(KEY_FOLDER, szComDate);
				//}
				//property_set(KEY_DATE_PREPARE, "1");
				return 0;
				//}
			}
			sleep(1);
		}
		SLOGI("property always write error");
		return 0;
	}
	/*long starttime=getuptime();
	if(starttime<90){// fix bug  getprop date is incorrect in init_log_date ,but error in this deamon at init system
		char szChenkDate[PROPERTY_VALUE_MAX]={0};
		property_get(KEY_TEMP_DATE,szChenkDate,"");
		property_get(KEY_TEMP_SDATE,szDate,"");
		if(strlen(szChenkDate)==0 || strcmp(szChenkDate,szDate)!=0){
			SLOGI("property not ready ,szChenkDate=%s,szDate=%s",szChenkDate,szDate);
			exit(-1);
		}
	}

	if(isUserBuild() == false){
		property_get("sys.asuslog.media.prepare",szMediaPrepare,"");
		if(strcmp("1",szMediaPrepare) != 0){
			SLOGI("/data/media not ready");
			sleep(5);
			property_set("sys.asuslog.media.prepare", "1");
		}
	}

	int date_count=0;
	do{
		property_get(KEY_DATE_PREPARE, szDatePrepare,"0");
		date_count++;
		if(strcmp("0",szDatePrepare) == 0){
			sleep(1);
		}
	}while(strcmp("0",szDatePrepare) == 0 && date_count <= 30);*/

	int mode=-1;
	if(argc==1){
		printf("error parameter is too few");
		exit(-1);
	}else if(argc==2 && 0 == strcmp(argv[1], "main")){
		SLOGI("main log");
		mode=MAIN_LOG;
	}else if(argc==2 && 0 == strcmp(argv[1], "kernel")){
		SLOGI("kernel log");
		mode=KERNEL_LOG;
	}else if(argc==2 && 0 == strcmp(argv[1], "radio")){
		SLOGI("radio log");
		mode=RADIO_LOG;
	}else if(argc==2 && 0 == strcmp(argv[1], "event")){
		SLOGI("event log");
		mode=EVENT_LOG;
	}else if(argc==2 && 0 == strcmp(argv[1], "combine")){
		SLOGI("combine log");
		mode=COMBINE_LOG;
	}else if(argc==2 && 0 == strcmp(argv[1], "tcpdump")){
		SLOGI("tcp log");
		mode=TCP_DUMP;
	}else{
		printf("error parameter error");
		exit(-1);
	}

	getLogRootPath(gszLogRootDir);

	char storePath[BUF_SIZE]={0};

	int returnCode=createDir(gszLogRootDir,false,false);
	SLOGI("gszLogRootDir=%s,returnCode=%d",gszLogRootDir,returnCode);
	sprintf(szDebug,"echo createDir_path=%s,returncode=%d >> /data/logtool_dump.txt",gszLogRootDir,returnCode);
	system(szDebug);

	if(returnCode==ERROR_BOOTSTRAP_UNMOUNT){
		//waiting mkdir success //user mode discovery boot mount slowly
		exit(-1);
		//createDirWaitMount(gszLogRootDir);
	}
	if(mode==TCP_DUMP && isSaveMicroSD()==false){
		sprintf(gszLogDir,	"%s%s/"		,gszLogRootDir,DIR_NAME_TCPDUMP);
	}else{
		getDumpDate(szDate);
		SLOGI("strlen=%d",strlen(szDate));
		//if(strlen(szDate)==0 || strlen(szDate) < 20)
		if(strlen(szDate)==0)
		{
			SLOGI("date no ready leave");
			exit(-1);
		}
		getComDate(szComDate);
		getIndex(szIndex);
		if(strlen(szComDate) == 0 || strlen(szIndex) == 0)
		{
			SLOGI("folder name no ready leave");
			exit(-1);
		}
		int ind = atoi(szIndex);
		char oldIndex[16]={0};
		sprintf(oldIndex, "%03d", ind - 1);
		if(strncmp(oldIndex, szComDate, 3) != 0)
		{
			SLOGI("%s %s",oldIndex, szComDate);
			SLOGI("Index different leave");
			exit(-1);
		}
		sprintf(gszLogDir,		"%s%s/"		,gszLogRootDir, szComDate);
		SLOGI("gszLogDir=%s",gszLogDir);
	}

	returnCode=createDir(gszLogDir,false,false);
	SLOGI("gszLogDir=%s,returnCode=%d",gszLogDir,returnCode);
	if(returnCode==ERROR_BOOTSTRAP_UNMOUNT){
		exit(-1);
	}


	int execReturn=0;
	switch(mode){
	case MAIN_LOG:
		execReturn=execLogCmd("Main", "logcat -b main -b system  %s -v uptime -f",storePath,gszLogDir,DEFAULT_LOG_SIZE);
		break;
	case KERNEL_LOG:
		if(isFileExist("/dev/log/kernel")==false){

			if(isFileExist("/system/bin/logkmsg")==false){
				SLOGI("*******************************/proc/kmsg log");
				onHandelLogPath(false, storePath,gszLogDir,"Kernel");
				execReturn=execCmd("cat /proc/kmsg > ",storePath);
			}else{
				SLOGI("*******************************logkmsg");
				execReturn=execLogCmd("Kernel", "asuslogcat -b kernel  %s -v uptime -f",storePath,gszLogDir,DEFAULT_LOG_SIZE);
			}
		}else {
			execReturn=execLogCmd("Kernel", "logcat -b kernel  %s -v uptime -f",storePath,gszLogDir,DEFAULT_LOG_SIZE);
		}

		break;
	case RADIO_LOG:
		execReturn=execLogCmd("Radio", "logcat -b radio  %s -v uptime -f",storePath,gszLogDir,DEFAULT_LOG_SIZE);
		break;
	case EVENT_LOG:
		execReturn=execLogCmd("Event", "logcat -b events  %s -v uptime -f",storePath,gszLogDir,DEFAULT_LOG_SIZE);
		break;
	case COMBINE_LOG:

		property_get(KEY_COMBINE_CONFIG,szValue,"");
		if(strlen(szValue)==0){//select all
			execReturn=execLogCmd("Combine", "logcat -b main -b system -b events -b kernel -b radio %s -v uptime -f",storePath,gszLogDir,DEFAULT_LOG_SIZE);
		}else{
			int digit=atoi(szValue);
			char szCombincmd[260]="logcat";
			bool mainLog=((digit&LOG_COMBINE_MAIN)==LOG_COMBINE_MAIN)?true:false;
			bool systemLog=((digit&LOG_COMBINE_SYSTEM)==LOG_COMBINE_SYSTEM)?true:false;
			bool kernelLog=((digit&LOG_COMBINE_KERNEL)==LOG_COMBINE_KERNEL)?true:false;
			bool radioLog=((digit&LOG_COMBINE_RADIO)==LOG_COMBINE_RADIO)?true:false;
			bool eventsLog=((digit&LOG_COMBINE_EVENTS)==LOG_COMBINE_EVENTS)?true:false;
			if(mainLog){
				strcat(szCombincmd," -b main");
			}
			if(systemLog){
				strcat(szCombincmd," -b system");
			}
			if(kernelLog){
				strcat(szCombincmd," -b kernel");
			}
			if(radioLog){
				strcat(szCombincmd," -b radio");
			}
			if(eventsLog){
				strcat(szCombincmd," -b events");
			}
			strcat(szCombincmd," %s -v uptime -f");
			execReturn=execLogCmd("Combine", szCombincmd,storePath,gszLogDir,80000);
		}

		break;
	case TCP_DUMP:
		char szTcpDumpPath[PATH_SIZE]={0};
		char szDate[PATH_SIZE]={0};
		getDate(szDate,PATH_SIZE);
		sprintf(szTcpDumpPath,	"%s%s_%s.log"		,gszLogDir,FILE_NAME_TCPDUMP,szDate);
		execReturn=execCmd("tcpdump -i any -p -s 0 -W 2 -C 100 -w",szTcpDumpPath);
		break;
	}
	SLOGI("leave mode:%d,execReturn=%d",mode,execReturn);
	sprintf(szDebug,"echo execCmd_returncode=%d >> /data/logtool_dump.txt",execReturn);
	system(szDebug);
}



int execLogCmd(char * logOpt, char *cmd,char *storePath,char * gszLogDir,int size){
	char szLogCmd[BUF_SIZE]={0};
	char rotatesize[BUF_SIZE]={0};
		if(getBuildSDK()==SDK_4_3){
			sprintf(rotatesize,"%s %d","-r",size);//-r 20000
		}else{
			sprintf(rotatesize,"%s%d","-r",size);//-r20000
		}
	onHandelLogPath(false, storePath,gszLogDir,logOpt);
	sprintf(szLogCmd,cmd,rotatesize);
	int result=execCmd(szLogCmd,storePath);
	return result;
}


void dumpLog(char* data){
	char szDebug[255]={0};
	sprintf(szDebug,"echo %s >> /data/logtool_dump.txt",data);
	system(szDebug);
}
