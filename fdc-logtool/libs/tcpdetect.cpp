#include <cutils/log.h>
#include <cutils/properties.h>
#include <errno.h>
#include "logtool.h"
#pragma GCC diagnostic ignored "-Wwrite-strings"
#define LOG_TAG "LogTool"
#define DIR_NAME_TCPDUMP	"TcpDump"
#define DIR_NAME_MODEM		"Modem"



#define KEY_SERVICE_MODE_ENABLE "persist.service.mtsp.enable"
#define KEY_SERVICE_MTS_ROTATE_NUM "persist.service.mts.rotate_num"
#define KEY_MODEM_SIZE "persist.asuslog.modem.size"
#define KEY_MODEM_COUNT "persist.asuslog.modem.count"

char gszTcpDumpPath[PATH_SIZE]={0};
char gszModemPath[PATH_SIZE]={0};

char gszLogRootDir[BUF_SIZE]={0};
char gszTcpDumpDir[PATH_SIZE]={0};
char gszModemDir[PATH_SIZE]={0};
pid_t gTcpDumpPid=0;
bool gbUpdateMtpPath=true;

bool gbTcpWaitMount=false;
bool gbModemWaitMount=false;


bool isTcpDumpSupport()
{
	char szValue[256]={0};
	property_get(KEY_ENABLE_TCPDUMP,szValue,"");
	if(strcmp(szValue,ENABLE)==0){
		return true;
	}
	return false;
}

bool isModemDumpSupport()
{
	char szValue[256]={0};
	property_get(KEY_ENABLE_COREDUMP,szValue,"");
	if(strcmp(szValue,ENABLE)==0){
		return true;
	}

	return false;
}

void startTcpDump()
{

	if(isMicroStrorage(gszTcpDumpDir))
	{//micro sdcard modem save new folder
		onHandelLogPath( gbTcpWaitMount,gszTcpDumpPath, gszTcpDumpDir, FILE_NAME_TCPDUMP );
		gbTcpWaitMount=false;

	}else if(isDataStrorage(gszModemDir))//wait microSD Mount
	{//save new folder
		onHandelLogPath( gbTcpWaitMount,gszTcpDumpPath, gszTcpDumpDir, FILE_NAME_TCPDUMP );
	}
	else
	{//externel sdcard tcp save same folder
		sprintf(gszTcpDumpPath,	"%s%s"		,gszTcpDumpDir,FILE_NAME_TCPDUMP);
	}


	gbTcpWaitMount=false;
	gTcpDumpPid=fork();


	if(gTcpDumpPid==0)
	{
		SLOGE("gszTcpDumpPath %s",gszTcpDumpPath);
		execCmd("tcpdump -i any -p -s 0 -W 2 -C 100 -w",gszTcpDumpPath);

		exit(0);
	}

}

void closeModemDump(){
	property_set("persist.service.mts.output", "");
	property_set("persist.service.mts.output_type", "");
	property_set(KEY_SERVICE_MTS_ROTATE_NUM, "");
	property_set("persist.service.mts.rotate_size", "");
	property_set("persist.service.mts.name", "");
	property_set(KEY_SERVICE_MODE_ENABLE, "0");
}

void triggerModemStart(){
	property_set("persist.service.mts.input", "/dev/mdmTrace");
	int result=system("echo -e  \"at+xsio=3\r\n\" > /dev/gsmtty19");
	sleep(5);//Reset modem
	int result2=system("echo -e  \"at+cfun=15\r\n\" > /dev/gsmtty19");
	sleep(30);

	int result3=system("echo -e \"at+trace=,115200,\\\"st=1,pr=1,bt=1,ap=0,db=1,lt=0,li=1,ga=0,ae=0\\\"\r\n\"> /dev/gsmtty19");
	sleep(5);
	int result4=system("echo -e \"at+xsystrace=0,\\\"bb_sw=1;3g_sw=1;digrfx=1;3g_dsp=1\\\",\\\"bb_sw=sdl:Si,tr,pr,st,db,lt,li,gt;digrfx=0x03\\\",\\\"oct=4\\\"\r\n\" > /dev/gsmtty19");
	sleep(5);
}

void startModemDump()
{
	char szSize[BUF_SIZE]={0};
	char szCount[BUF_SIZE]={0};
	closeModemDump();// when running record log ,reboot system,the persist.service.mts.output is preview config,so log is wrong path

	property_get(KEY_MODEM_SIZE,	szSize,"200000");
	property_get(KEY_MODEM_COUNT,	szCount,"3");
	if(isMicroStrorage(gszModemDir))
	{//micro sdcard modem save new folder
		onHandelLogPath( gbModemWaitMount,gszModemPath, gszModemDir, FILE_NAME_BPLOG );
		gbModemWaitMount=false;

	}else if(isDataStrorage(gszModemDir))//wait microSD Mount
	{//save new folder
		onHandelLogPath( gbModemWaitMount,gszModemPath, gszModemDir, FILE_NAME_BPLOG );
		strcpy(szCount,"1");
	}
	else
	{//externel sdcard modem save same folder
		sprintf(gszModemPath,	"%s%s"		,gszModemDir,FILE_NAME_BPLOG);

	}
	SLOGI(" gszModemPath %s",gszModemPath);
	property_set("persist.service.mts.output", gszModemPath);
	property_set("persist.service.mts.output_type", "f");

	property_set(KEY_SERVICE_MTS_ROTATE_NUM,szCount );
	property_set("persist.service.mts.rotate_size", szSize);
	property_set("persist.service.mts.name", "mtsfs");
	property_set(KEY_SERVICE_MODE_ENABLE, "1");

}


void setAllLogDir(char * rootlogdir){
	sprintf(gszTcpDumpDir,	"%s%s/"		,rootlogdir,DIR_NAME_TCPDUMP);
	sprintf(gszModemDir,	"%s%s/"		,rootlogdir,DIR_NAME_MODEM);

}

bool init(bool isChangePath){
	SLOGI("tcp init");
	getLogRootPath(gszLogRootDir);
	int returnCode=createDir(gszLogRootDir,false,gbUpdateMtpPath);
	bool result=true;

	if(isMicroStrorage(gszLogRootDir) && returnCode==ERROR_BOOTSTRAP_UNMOUNT){
		//write log
		if(isChangePath){
			createDirWaitMount(gszLogRootDir);
		}else{
			setBootMountEnable(false);
			sprintf(gszLogRootDir,"%s",DIR_MICRO_WAIT_MOUNT_DIR);
			createDir(gszLogRootDir,false,gbUpdateMtpPath);
			result=false;
		}

	}else if(returnCode==ERROR_BOOTSTRAP_UNMOUNT){
		//waiting mkdir success //user mode discovery boot mount slowly
		createDirWaitMount(gszLogRootDir);
	}

	gbUpdateMtpPath=isMtpUpdate(gszLogRootDir);
	setAllLogDir(gszLogRootDir);
	createDirWaitMount(gszTcpDumpDir);
	createDirWaitMount(gszModemDir);

	return result;
}

void closeTcpDump(){
	if(gTcpDumpPid!=0){
		kill(gTcpDumpPid,SIGINT);
		gTcpDumpPid=0;
		execCmd("chmod -R 777"	,	gszTcpDumpDir);
		if(gbUpdateMtpPath){
			broadcastMountPath(gszTcpDumpDir);
		}
	}
}


void listenCallback(char * name,char *value){

	if(strcmp(name,KEY_ENABLE_TCPDUMP)==0)
	{
		if(checkEnble(value))
		{
			if(gTcpDumpPid==0){
				startTcpDump();
			}
		}else{
			closeTcpDump();
		}
	}
	else if(strcmp(name,KEY_ENABLE_COREDUMP)==0){
		if(checkEnble(value)){
			startModemDump();
		}else{
			closeModemDump();
			execCmd("chmod -R 777"	,	gszModemDir);
		}
	}
	else if(strcmp(name,LOG_PATH_CHANGE)==0){//when write to micro sd ,may be mkdir error 65280
		bool bTcpDump=isTcpDumpSupport();
		bool bModemDump=isModemDumpSupport();
		if(bTcpDump){
			closeTcpDump();
		}
		if(bModemDump){
			closeModemDump();
		}
		init(true);
		if(bTcpDump){
			startTcpDump();
		}
		if(bModemDump){
			startModemDump();
		}
	}

}

void* waitMountRun(void * arg){
	SLOGI("tcp modem waitMountRun before");
	waitMount(true,gszLogRootDir);
	//mount
	if(isSaveMicroSD()){
		getLogRootPath(gszLogRootDir);
		SLOGI("tcp modem waitMountRun after");

		init(false);
		bool diskSpaceValid=checkMemorySafe(gszLogRootDir);
		if(isModemDumpSupport() && diskSpaceValid){
			gbModemWaitMount=true;
			startModemDump();
		}
		if(isTcpDumpSupport() && diskSpaceValid)
		{
			gbTcpWaitMount=true;
			closeTcpDump();
			startTcpDump();
		}

	}
	return 0;
}

int main(int argc, char *argv[])
{
	if(LOG_DEBUG==true){
		SLOGE("tcp main");
	}

	bool result=init(false);
	bool bTcpDump=isTcpDumpSupport();
	bool bModemDump=isModemDumpSupport();
	char szlogRoot[PATH_SIZE]={0};
	getLogRootPath(szlogRoot);
	bool diskSpaceValid=checkMemorySafe(gszLogRootDir);
	if(bTcpDump && diskSpaceValid)
	{
		startTcpDump();
	}
	if(bModemDump && diskSpaceValid && isSaveMicroSD()){
		startModemDump();
	}
	if(result==false){
		pthread_t id;
		int ret=pthread_create(&id,NULL,waitMountRun,NULL);
		 if(ret!=0){
			 printf ("Create pthread error!\n");
			 exit (1);
		 }
	}
	bool bDynamicTcpDump=isTcpDumpSupport();
	bool bDynamicModemDump=isModemDumpSupport();
	char szDynamiclogRoot[PATH_SIZE]={0};
	getLogRootPath(szDynamiclogRoot);
	while(true)
	{
		if(( bDynamicTcpDump=isTcpDumpSupport())!=bTcpDump)
		{
			if(bDynamicTcpDump==true){
				listenCallback(KEY_ENABLE_TCPDUMP,"1");
			}else{
				listenCallback(KEY_ENABLE_TCPDUMP,"0");
			}

			bTcpDump=bDynamicTcpDump;
		}

		//listen log path change
		getLogRootPath(szDynamiclogRoot);
		if(strcmp(szDynamiclogRoot,szlogRoot)!=0){
			listenCallback(LOG_PATH_CHANGE,szDynamiclogRoot);
			sprintf(szlogRoot,"%s",szDynamiclogRoot);
		}

		sleep(1);
	}
	if(LOG_DEBUG==true){
		SLOGE("tcp leave");
	}

	return 0;
}
