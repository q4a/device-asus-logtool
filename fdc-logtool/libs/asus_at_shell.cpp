#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <errno.h>
#define GSMTTY_PATH "/dev/gsmtty19"

#define TTY_CLOSED -1
int opentty();
int closeSerial(int fd) ;
bool file_exists(const char * filename);
bool DEBUG=false;
int main(int argc, char **argv) {
	int fd;
	int ret = 0;


	if(file_exists(GSMTTY_PATH)==false)
	{
		printf("/dev/gsmtty19 is already died\n");
		exit(-1);
	}

	if(strcmp("-h",argv[1])==0){
			printf("argument -c ,full command input,including at+,ex:[at+creg?]\n");
			printf("argument -f , Autocomplete [at+],don't input [at+], ex:[creg?]\n");
			exit(-1);
	}

	if(argc!=3)
	{
			printf("input parameter is too few\n");
			exit(-1);
	}

	char cmd_buffer[256]={0};
	if(strcmp("-f",argv[1])==0){
		sprintf(cmd_buffer,"at+%s\r",argv[2]);
	}else if(strcmp("-c",argv[1])==0){
		sprintf(cmd_buffer,"%s\r",argv[2]);
	}else{
		printf("input parameter error\n");
		exit(-1);
	}


	//char cmd_buffer[] = "AT\n\r";
	//char cmd_buffer[] = "AT+XSYSTRACE=10\r";
	//printf("input:%s\n",cmd_buffer);
	char res_buffer[256]={0};

	fd = opentty();
	if (fd < 0) {
		printf("at_shell: open failed\n\r");
		return -1;
	}

	//printf("at_shell: open ok fd id= %d\n\r",fd);

	ret = write(fd, cmd_buffer, sizeof(cmd_buffer));
	if (ret <= 0) {
		printf("at_shell: write cmd %s (size %d) failed, ret = %d\n\r",
				cmd_buffer, sizeof(cmd_buffer), ret);
		return -2;
	}

	printf("at_shell: write cmd %s (size %d) ok\n\r", cmd_buffer,sizeof(cmd_buffer));

	ret = read(fd, res_buffer, sizeof(res_buffer));
	if (ret < 0) {
		printf("at_shell: read failed, ret = %d\n\r", ret);
		return -3;
	}
	if(DEBUG==true){
		printf("at_shell: read ok, res %s (0x%x 0x%x 0x%x 0x%x)\n\r", res_buffer,
					res_buffer[0], res_buffer[1], res_buffer[2], res_buffer[3]);
	}
	printf("%s", res_buffer);
	closeSerial(fd);
	return 0;

}

bool file_exists(const char * filename)
{
    if (FILE * file = fopen(filename, "r"))
    {
        fclose(file);
        return true;
    }
    return false;
}

int opentty()
{
    int fd = TTY_CLOSED;

    struct termios tio;

    //| CLOCAL
    fd = open(GSMTTY_PATH, O_RDWR | O_NOCTTY );
    if (fd < 0) {
        printf("OpenSerial: %s (%d)", strerror(errno), errno);
        goto open_serial_failure;
    }

    struct termios terminalParameters;
    if (tcgetattr(fd, &terminalParameters)) {
    	printf("OpenSerial: %s (%d)", strerror(errno), errno);
        goto open_serial_failure;
    }

    cfmakeraw(&terminalParameters);
    if (tcsetattr(fd, TCSANOW, &terminalParameters)) {
    	printf("OpenSerial: %s (%d)", strerror(errno), errno);
        goto open_serial_failure;
    }


    memset(&tio, 0, sizeof(tio));
    tio.c_cflag = B115200;
    tio.c_cflag |= CS8 | CLOCAL | CREAD;
    tio.c_iflag &= ~(INPCK | IGNPAR | PARMRK | ISTRIP | IXANY | ICRNL);
    tio.c_oflag &= ~OPOST;
    tio.c_cc[VMIN] = 1;
    tio.c_cc[VTIME] = 10;

    tcflush(fd, TCIFLUSH);
    cfsetispeed(&tio, 115200);
    tcsetattr(fd, TCSANOW, &tio);

    goto open_serial_success;

open_serial_failure:
    if (fd >= 0) {
        close(fd);
        fd = TTY_CLOSED;
    }

open_serial_success:
    if (fd != TTY_CLOSED)
    	printf("OpenSerial: %s opened (%d)", GSMTTY_PATH, fd);

    return fd;
}


int closeSerial(int fd) {

	if (fd >= 0) {
		close(fd);
		fd = TTY_CLOSED;
	}
	return 0;
}
