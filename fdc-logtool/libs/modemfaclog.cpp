#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <termios.h>
#include <errno.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include "logtool.h"
#define GSMTTY_PATH "/dev/gsmtty19"
#define LOG_TAG "LogTool"
#define TTY_CLOSED -1
int opentty();
int closeSerial(int fd) ;
bool file_exists(const char * filename);
void startService(char *path);
int sendCommand(int fd,char* cmd_buffer,char *success);
bool DEBUG=false;
bool g_bDumpShell=false;
#define FAIL -1
#define SUCCESS 0
#define CMDLEN 3
#define CLOSE_CMD_LEN 2
char* g_openATCmd[CMDLEN]={"at+xsystrace=0,\"bb_sw=1;3g_sw=1;digrfx=0\",\"bb_sw=sdl:Si,tr,pr,st,db,lt,li,gt\",\"oct=4\"\r",
						"AT+TRACE=1\r",
						"at+xsystrace=10\r"};
char* g_closeATCmd[CLOSE_CMD_LEN]={"AT+TRACE=0\r",
						"AT+XSYSTRACE=0\r"};
char* g_pszATSuccess[CMDLEN]={"OK","OK","Oct"};
void closeService();
void closeModemLog(int fd);
void dump(const char *fmt, ...);
int main(int argc, char **argv) {
	int fd;
	dump("=========modem log main=============2\n");
	if(file_exists(GSMTTY_PATH)==false)
	{
		dump("/dev/gsmtty19 is already died\n");
		exit(-1);
	}

	fd = opentty();
	if (fd < 0) {
		dump("at_shell: open failed\n\r");
		return -1;
	}

	if(argc==2)//close
	{

		if(strcmp("close",argv[1])==0){
			dump("close log start\n");
			closeModemLog(fd);
			closeService();
			exit(0);
		}
	}

	bool success=false;
	for(int i=0;i<CMDLEN;i++){
		int result=sendCommand( fd,g_openATCmd[i],g_pszATSuccess[i]);
		if(result!=SUCCESS){
			break;
		}
		if(i==CMDLEN-1){
			success=true;
		}
	}
	closeSerial(fd);
	if(success==false){
		dump("log open fail at command fail");
		exit(-1);
	}
	char szLogRootDir[PATH_SIZE]={0};
	char szLogDir[PATH_SIZE]={0};
	getLogRootPath(szLogRootDir);

	sprintf(szLogDir,	"%s%s/"		,szLogRootDir,"Modem");
	int returnCode=createDir(szLogDir,false,false);
	dump("gszLogDir=%s,returnCode=%d",szLogDir,returnCode);
	 if(returnCode==ERROR_BOOTSTRAP_UNMOUNT){
		 exit(-1);
	 }
	char path[PATH_SIZE]={0};
	sprintf(path,"%s/bplog",szLogDir);
	startService(path);
	property_set("persist.asuslog.facmodeminit", "0");
	return 0;

}


void dump(const char *fmt, ...)
{

	int ret;
	va_list ap;
	va_start(ap, fmt);
	if(g_bDumpShell==true){
		ret = vfprintf(stdout, fmt, ap);
	}else{
		char szBuffer[512]={0};
		vsprintf(szBuffer,fmt,ap);
		SLOGI(szBuffer);
	}
	va_end(ap);
}

void closeModemLog(int fd){
	bool success=false;
		for(int i=0;i<CLOSE_CMD_LEN;i++){
			int result=sendCommand( fd,g_closeATCmd[i],"OK");
			if(result!=SUCCESS){
				break;
			}
			if(i==CLOSE_CMD_LEN-1){
				success=true;
			}
		}
		closeSerial(fd);
		if(success==false){
			dump("===log close fail===");
			exit(-1);
		}
}

void startService(char *path)
{
	property_set("persist.service.mts.input", "/dev/mdmTrace");
	property_set("persist.service.mts.output_type", "f");
	property_set("persist.service.mts.rotate_size", "20000");
	property_set("persist.service.mts.rotate_num", "3");
	property_set("persist.service.mts.name" ,"mtsfs");
	property_set("persist.service.mts.output" ,path);
	property_set("persist.asuslog.fdcmts.enable", "1");
}


void closeService()
{
	property_set("persist.service.mts.input", "");
	property_set("persist.service.mts.output_type", "");
	property_set("persist.service.mts.rotate_size", "");
	property_set("persist.service.mts.rotate_num", "");
	property_set("persist.service.mts.name" ,"");
	property_set("persist.service.mts.output" ,"");
	property_set("persist.asuslog.fdcmts.enable", "0");
}

int sendCommand(int fd,char* cmd_buffer,char *success)
{
	char res_buffer[256]={0};
	int ret = write(fd, cmd_buffer, strlen(cmd_buffer));
		if (ret <= 0) {
			dump("at_shell: write cmd %s (len %d) failed, ret = %d\n\r",cmd_buffer, strlen(cmd_buffer), ret);
			return -2;
		}

	dump("at write cmd %s (len %d) \n\r", cmd_buffer,strlen(cmd_buffer));

	ret = read(fd, res_buffer, sizeof(res_buffer));
	if (ret < 0) {
		dump("at read failed, ret = %d\n\r", ret);
		return -3;
	}

	dump("at response len=%d,result={%s}\n",ret ,res_buffer);
	if(strstr (res_buffer,success)!=NULL){

		return SUCCESS;
	}

	return FAIL;
}

bool file_exists(const char * filename)
{
    if (FILE * file = fopen(filename, "r"))
    {
        fclose(file);
        return true;
    }
    return false;
}

int opentty()
{
    int fd = TTY_CLOSED;

    struct termios tio;

    //| CLOCAL | O_NOCTTY
    fd = open(GSMTTY_PATH, O_RDWR  );
    if (fd < 0) {
        printf("OpenSerial: %s (%d)", strerror(errno), errno);
        return TTY_CLOSED;
    }

    struct termios terminalParameters;
    if (tcgetattr(fd, &terminalParameters)) {
    	printf("OpenSerial: %s (%d)", strerror(errno), errno);
        goto open_serial_failure;
    }

    cfmakeraw(&terminalParameters);
    if (tcsetattr(fd, TCSANOW, &terminalParameters)) {
    	printf("OpenSerial: %s (%d)", strerror(errno), errno);
        goto open_serial_failure;
    }


    memset(&tio, 0, sizeof(tio));
    tio.c_cflag = B115200;
    tio.c_cflag |= CS8 | CLOCAL | CREAD;
    tio.c_iflag &= ~(INPCK | IGNPAR | PARMRK | ISTRIP | IXANY | ICRNL);
    tio.c_oflag &= ~OPOST;
    tio.c_cc[VMIN] = 1;
    tio.c_cc[VTIME] = 10;

    tcflush(fd, TCIFLUSH);
    cfsetispeed(&tio, 115200);
    tcsetattr(fd, TCSANOW, &tio);

    goto open_serial_success;

open_serial_failure:
    if (fd >= 0) {
        close(fd);
        fd = TTY_CLOSED;
    }

open_serial_success:
    if (fd != TTY_CLOSED)
    	printf("OpenSerial: %s opened (%d)", GSMTTY_PATH, fd);

    return fd;
}


int closeSerial(int fd) {

	if (fd >= 0) {
		close(fd);
		fd = TTY_CLOSED;
	}
	return 0;
}
